/********************************************************************************/
/* Portable Graphics Library for Embedded Systems * (C) Componentality Oy, 2016 */
/* Initial design and development: Konstantin A. Khait                          */
/* Support, comments and questions: dev@componentality.com                      */
/********************************************************************************/
/* Layouts help arranging graphic elements on the screen without changing       */
/* of coordinates directly                                                      */
/********************************************************************************/

#ifndef __LAYOUT_H__
#define __LAYOUT_H__

#include "../Surface/easy_draw.h"
#include "../JetCat/placement.h"

namespace Componentality
{
	namespace Graphics
	{
		namespace JetCat
		{
			// Basic interface for layout
			class ILayout : public Placement
			{
			protected:
				bool mStretch;	// Stretch content to layout size
				bool mWAdjust;	// Adjust layout size to content (width)
				bool mHAdjust;	// Adjust layout size to content (height)
			public:
				ILayout(const bool stretch = false, const bool wadjust = false, const bool hadjust = false) : mStretch(stretch), mWAdjust(wadjust), mHAdjust(hadjust) {};
				virtual ~ILayout() {};
				virtual void setStretch(const bool stretch = false) { mStretch = stretch; }
				virtual bool getStretch() const { return mStretch; }
			protected:
				virtual void autosize(ISurface&);
			};

			// Linear layout makes simple list of placements
			class ILinearLayout : public ILayout
			{
			protected:
				struct PLACEMENT
				{
					Placement* mPlacement;
					size_t     mShift;
					size_t	   mPadding;
					size_t	   mOriginalWidth;		// Width and height before stretching
					size_t	   mOriginalHeight;
					double     mWeight;
					PLACEMENT() { mPlacement = NULL; mOriginalWidth = mOriginalHeight = mShift = ____UNDEFINED; mPadding = 0; mWeight = 0.0; }
					virtual ~PLACEMENT() {};
				};
				std::list<PLACEMENT> mPlacements;
			public:
				ILinearLayout(const bool stretch = false, const bool wadjust = false, const bool hadjust = false) : ILayout(stretch, wadjust, hadjust) {};
				virtual ~ILinearLayout() {};
			public:
				virtual void add(const Tag tag, Placement&, const size_t shift = ____UNDEFINED, const size_t padding = 0);
			protected:
				virtual void add(const Tag, IElement&);
				virtual void add(const Tag tag, Placement& placement, const double weight, const size_t padding = 0);
			};

			class VerticalLayout : public ILinearLayout
			{
			public:
				enum GRAVITY { TOP, BOTTOM, CENTER };
			protected:
				GRAVITY mGravity;
			public:
				VerticalLayout(const bool stretch = false, const bool wadjust = false, const bool hadjust = false) : ILinearLayout(stretch, wadjust, hadjust) { mGravity = TOP; };
				virtual ~VerticalLayout() {};
				virtual void setGravity(const GRAVITY gravity) { mGravity = gravity; };
				virtual GRAVITY getGravity() const { return mGravity; }
			protected:
				virtual Margins draw(ISurface&);
			};

			class HorizontalLayout : public ILinearLayout
			{
			public:
				enum GRAVITY {LEFT, RIGHT, CENTER};
			protected:
				GRAVITY mGravity;
			public:
				HorizontalLayout(const bool stretch = false, const bool wadjust = false, const bool hadjust = false) : ILinearLayout(stretch, wadjust, hadjust) { mGravity = LEFT; };
				virtual ~HorizontalLayout() {};
				virtual void setGravity(const GRAVITY gravity) { mGravity = gravity; };
				virtual GRAVITY getGravity() const { return mGravity; }
			protected:
				virtual Margins draw(ISurface&);
			};

			class ProportionalVerticalLayout : public ILinearLayout
			{
			protected:
				size_t mTotalFixedSize;					// Total dimensions of all elements with fixed size in pixels
				double mTotalWeight;					// Total weight of app proportional elements
			public:
				ProportionalVerticalLayout(const bool stretch = false, const bool wadjust = false, const bool hadjust = false) : ILinearLayout(stretch, wadjust, hadjust) { mTotalFixedSize = 0; mTotalWeight = 0.0; };
				virtual ~ProportionalVerticalLayout() {};
				virtual void add(const Tag tag, Placement& placement, const size_t padding = 0);
				virtual void add(const Tag tag, Placement& placement, const double weight, const size_t padding = 0);
			protected:
				virtual Margins draw(ISurface&);
			};

			class ProportionalHorizontalLayout : public ILinearLayout
			{
			protected:
				size_t mTotalFixedSize;					// Total dimensions of all elements with fixed size in pixels
				double mTotalWeight;					// Total weight of app proportional elements
			public:
				ProportionalHorizontalLayout(const bool stretch = false, const bool wadjust = false, const bool hadjust = false) : ILinearLayout(stretch, wadjust, hadjust) { mTotalFixedSize = 0; mTotalWeight = 0.0; };
				virtual ~ProportionalHorizontalLayout() {};
				virtual void add(const Tag tag, Placement& placement, const size_t padding = 0);
				virtual void add(const Tag tag, Placement& placement, const double weight, const size_t padding = 0);
			protected:
				virtual Margins draw(ISurface&);
			};

			class FrameLayout : public ILayout
			{
			protected:
				Margins mMargins;
				Placement* mObject;
			public:
				FrameLayout(const Margins margins = Margins(), const bool stretch = true, const bool wadjust = false, const bool hadjust = false) : mMargins(margins), ILayout(stretch, wadjust, hadjust) {};
				virtual ~FrameLayout() {};
				virtual void add(Placement& object) { mObject = &object; ILayout::add("obj", object); };
			protected:
				virtual Margins draw(ISurface&);
			protected:
				virtual void add(const Tag, IElement&) {};
			};

			class GridLayout : public ILayout
			{
			public:
				typedef std::pair<size_t, size_t> INDEX;
			protected:
				std::map<INDEX, Placement*> mObjects;	// Matrix of objects
				size_t mCellWidth;						// Width of the cell
				size_t mCellHeight;						// Height of the cell
				size_t mHCells;							// Number of horizontal cells
				size_t mVCells;							// Number of vertical cells
			public:
				GridLayout(const size_t hcells, const size_t vcells, const size_t cell_width = ____UNDEFINED, const size_t cell_height = ____UNDEFINED,
					const bool stretch = true);
				virtual ~GridLayout();
				virtual void add(const size_t x, const size_t y, Placement& element);
			protected:
				virtual Margins draw(ISurface&);
			protected:
				virtual void add(const Tag tag, IElement& element) { return ILayout::add(tag, element); };
			};

			class AutoSizeLayout : public FrameLayout
			{
			public:
				AutoSizeLayout() {};
				virtual ~AutoSizeLayout() {};
			protected:
				virtual Margins draw(ISurface&);
			};

		}
	}
}

#endif

