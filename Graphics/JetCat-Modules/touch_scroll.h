/*************************************************************************************/
/* Portable Graphics Library for Embedded Systems * (C) Componentality Oy, 2015-2016 */
/* Initial design and development: Konstantin A. Khait                               */
/* Support, comments and questions: dev@componentality.com                           */
/*************************************************************************************/

#ifndef __TOUCH_SCROLL_H__
#define __TOUCH_SCROLL_H__

#include "touch.h"
#include "../JetCat/menu.h"

namespace Componentality
{

	namespace Graphics
	{
		
		namespace JetCat
		{

			class TouchMenu : public virtual Scroller, public EasyGesturesProcessor
			{
			protected:
				bool mPressReceived;
				bool mGestureReceived;
				bool mMenuEventReceived;
				bool mIssueMenuEvent;
				Point mPressPoint;
				MenuEvent mMenuEvent;
			public:
				TouchMenu();
				virtual ~TouchMenu() {};
			protected:
				virtual bool onEvent(CST::Common::Event&);
				virtual bool preProcessEvent(CST::Common::Event&);
				virtual void setTopLeft(const Componentality::Graphics::Point topleft) { Scroller::setTopLeft(topleft); EasyGesturesProcessor::setTopLeft(topleft); }
				virtual void setRightBottom(const Componentality::Graphics::Point rightbottom) { Scroller::setRightBottom(rightbottom); EasyGesturesProcessor::setRightBottom(rightbottom); }
			};

			class VerticalTouchMenu : public VerticalMenu, public TouchMenu
			{
			public:
				VerticalTouchMenu(IFocusController& fc, const size_t hsize) : VerticalMenu(fc, hsize) { mHorizontalAllowed = false; };
				virtual ~VerticalTouchMenu() {};
			protected:
				virtual bool onEvent(CST::Common::Event&);
				virtual Margins draw(ISurface& surface) { return VerticalMenu::draw(surface); };
				virtual void setTopLeft(const Componentality::Graphics::Point topleft) { VerticalMenu::setTopLeft(topleft); TouchMenu::setTopLeft(topleft); }
				virtual void setRightBottom(const Componentality::Graphics::Point rightbottom) { VerticalMenu::setRightBottom(rightbottom); TouchMenu::setRightBottom(rightbottom); }
			};

			class HorizontalTouchMenu : public HorizontalMenu, public TouchMenu
			{
			public:
				HorizontalTouchMenu(IFocusController& fc, const size_t hsize) : HorizontalMenu(fc, hsize) { mVerticalAllowed = false; };
				virtual ~HorizontalTouchMenu() {};
			protected:
				virtual bool onEvent(CST::Common::Event&);
				virtual Margins draw(ISurface& surface) { return HorizontalMenu::draw(surface); };
				virtual void setTopLeft(const Componentality::Graphics::Point topleft) { HorizontalMenu::setTopLeft(topleft); TouchMenu::setTopLeft(topleft); }
				virtual void setRightBottom(const Componentality::Graphics::Point rightbottom) { HorizontalMenu::setRightBottom(rightbottom); TouchMenu::setRightBottom(rightbottom); }
			};


		}; // namespace JetCat

	}; // namespace Graphics

}; // namespace Componentality

#endif