/*************************************************************************************/
/* Portable Graphics Library for Embedded Systems * (C) Componentality Oy, 2015-2016 */
/* Initial design and development: Konstantin A. Khait                               */
/* Support, comments and questions: dev@componentality.com                           */
/*************************************************************************************/

#include "png_surface.h"
#include "lodepng.h"
#include "bitmap/bmp.h"

using namespace Componentality::Graphics;

//Input image must be RGB buffer (3 bytes per pixel), but you can easily make it
//support RGBA input and output by changing the inputChannels and/or outputChannels
//in the function to 4.
void encodeBMP(std::vector<unsigned char>& bmp, const unsigned char* image, int w, int h)
{
	//3 bytes per pixel used for both input and output.
	int inputChannels = 3;
	int outputChannels = 3;

	//bytes 0-13
	bmp.push_back('B'); bmp.push_back('M'); //0: bfType
	bmp.push_back(0); bmp.push_back(0); bmp.push_back(0); bmp.push_back(0); //2: bfSize; size not yet known for now, filled in later.
	bmp.push_back(0); bmp.push_back(0); //6: bfReserved1
	bmp.push_back(0); bmp.push_back(0); //8: bfReserved2
	bmp.push_back(54 % 256); bmp.push_back(54 / 256); bmp.push_back(0); bmp.push_back(0); //10: bfOffBits (54 header bytes)

																						  //bytes 14-53
	bmp.push_back(40); bmp.push_back(0); bmp.push_back(0); bmp.push_back(0);  //14: biSize
	bmp.push_back(w % 256); bmp.push_back(w / 256); bmp.push_back(0); bmp.push_back(0); //18: biWidth
	bmp.push_back(h % 256); bmp.push_back(h / 256); bmp.push_back(0); bmp.push_back(0); //22: biHeight
	bmp.push_back(1); bmp.push_back(0); //26: biPlanes
	bmp.push_back(outputChannels * 8); bmp.push_back(0); //28: biBitCount
	bmp.push_back(0); bmp.push_back(0); bmp.push_back(0); bmp.push_back(0);  //30: biCompression
	bmp.push_back(0); bmp.push_back(0); bmp.push_back(0); bmp.push_back(0);  //34: biSizeImage
	bmp.push_back(0); bmp.push_back(0); bmp.push_back(0); bmp.push_back(0);  //38: biXPelsPerMeter
	bmp.push_back(0); bmp.push_back(0); bmp.push_back(0); bmp.push_back(0);  //42: biYPelsPerMeter
	bmp.push_back(0); bmp.push_back(0); bmp.push_back(0); bmp.push_back(0);  //46: biClrUsed
	bmp.push_back(0); bmp.push_back(0); bmp.push_back(0); bmp.push_back(0);  //50: biClrImportant

																			 /*
																			 Convert the input RGBRGBRGB pixel buffer to the BMP pixel buffer format. There are 3 differences with the input buffer:
																			 -BMP stores the rows inversed, from bottom to top
																			 -BMP stores the color channels in BGR instead of RGB order
																			 -BMP requires each row to have a multiple of 4 bytes, so sometimes padding bytes are added between rows
																			 */

	int imagerowbytes = outputChannels * w;
	imagerowbytes = imagerowbytes % 4 == 0 ? imagerowbytes : imagerowbytes + (4 - imagerowbytes % 4); //must be multiple of 4

	for (int y = h - 1; y >= 0; y--) //the rows are stored inversed in bmp
	{
		int c = 0;
		for (int x = 0; x < imagerowbytes; x++)
		{
			if (x < w * outputChannels)
			{
				int inc = c;
				//Convert RGB(A) into BGR(A)
				if (c == 0) inc = 2;
				else if (c == 2) inc = 0;
				bmp.push_back(image[inputChannels * (w * y + x / outputChannels) + inc]);
			}
			else bmp.push_back(0);
			c++;
			if (c >= outputChannels) c = 0;
		}
	}

	// Fill in the size
	bmp[2] = bmp.size() % 256;
	bmp[3] = (bmp.size() / 256) % 256;
	bmp[4] = (bmp.size() / 65536) % 256;
	bmp[5] = (unsigned char)(bmp.size() / 16777216);
}

//returns 0 if all went ok, non-0 if error
//output image is always given in RGBA (with alpha channel), even if it's a BMP without alpha channel
unsigned decodeBMP(std::vector<unsigned char>& image, unsigned& w, unsigned& h, const std::vector<unsigned char>& bmp)
{
	static const unsigned MINHEADER = 54; //minimum BMP header size

	if (bmp.size() < MINHEADER) return -1;
	if (bmp[0] != 'B' || bmp[1] != 'M') return 1; //It's not a BMP file if it doesn't start with marker 'BM'
	unsigned pixeloffset = bmp[10] + 256 * bmp[11]; //where the pixel data starts
													//read width and height from BMP header
	w = bmp[18] + bmp[19] * 256;
	h = bmp[22] + bmp[23] * 256;
	//read number of channels from BMP header
	if (bmp[28] != 24 && bmp[28] != 32) return 2; //only 24-bit and 32-bit BMPs are supported.
	unsigned numChannels = bmp[28] / 8;

	//The amount of scanline bytes is width of image times channels, with extra bytes added if needed
	//to make it a multiple of 4 bytes.
	unsigned scanlineBytes = w * numChannels;
	if (scanlineBytes % 4 != 0) scanlineBytes = (scanlineBytes / 4) * 4 + 4;

	unsigned dataSize = scanlineBytes * h;
	if (bmp.size() < dataSize + pixeloffset) return 3; //BMP file too small to contain all pixels

	image = std::vector<unsigned char>(w * h * 4);

	/*
	There are 3 differences between BMP and the raw image buffer for LodePNG:
	-it's upside down
	-it's in BGR instead of RGB format (or BRGA instead of RGBA)
	-each scanline has padding bytes to make it a multiple of 4 if needed
	The 2D for loop below does all these 3 conversions at once.
	*/
	for (unsigned y = 0; y < h; y++)
		for (unsigned x = 0; x < w; x++)
		{
			//pixel start byte position in the BMP
			unsigned bmpos = pixeloffset + (h - y - 1) * scanlineBytes + numChannels * x;
			//pixel start byte position in the new raw image
			unsigned newpos = 4 * y * w + 4 * x;
			if (numChannels == 3)
			{
				image[newpos + 0] = bmp[bmpos + 2]; //R
				image[newpos + 1] = bmp[bmpos + 1]; //G
				image[newpos + 2] = bmp[bmpos + 0]; //B
				image[newpos + 3] = 255;            //A
			}
			else
			{
				image[newpos + 0] = bmp[bmpos + 3]; //R
				image[newpos + 1] = bmp[bmpos + 2]; //G
				image[newpos + 2] = bmp[bmpos + 1]; //B
				image[newpos + 3] = bmp[bmpos + 0]; //A
			}
		}
	return 0;
}


bool PNGSurface::read(const std::string& file)
{
	std::vector<unsigned char> image; //the raw pixels
	unsigned width, height;

	unsigned error = lodepng::decode(image, width, height, file.c_str(), LCT_RGB, 8);

	if (error)
	{
		return false;
	}

	mWidth = width;
	mHeight = height;
	// Allocates memory for the bitmap
	unsigned int size = getMemorySize();
	if (mBuffer)
		CST_FREE(mBuffer);
	mBuffer = (char*)CST_ALLOC(size, "PNGSurface", "buffer");
	memset(mBuffer, 0, size);
	std::vector<unsigned char> bmp;
	encodeBMP(bmp, &image[0], width, height);
	memcpy(mBuffer, bmp.data(), size);
	mFileHeader = (Bitmap::PBITMAPFILEHEADER)(&mBuffer[0]);
	mInfoHeader = (Bitmap::PBITMAPINFOHEADER)(&mBuffer[0] + sizeof(Bitmap::BITMAPFILEHEADER));
	return true;
}

bool PNGSurface::write(const std::string& file)
{
	std::vector<unsigned char> bmp((unsigned char*)getMemory(), (unsigned char*)getMemory() + getMemorySize());
	std::vector<unsigned char> image;
	unsigned w = getWidth(), h = getHeight();
	unsigned error = decodeBMP(image, w, h, bmp);

	if (error)
	{
		return false;
	}

	std::vector<unsigned char> png;
	error = lodepng::encode(png, image, w, h);

	if (error)
	{
		return false;
	}

	lodepng::save_file(png, file.c_str());

	return true;
}

// Retrieve blob with PNG data
CST::Common::blob PNGSurface::get()
{
	std::vector<unsigned char> bmp((unsigned char*)getMemory(), (unsigned char*)getMemory() + getMemorySize());
	std::vector<unsigned char> image;
	unsigned w = getWidth(), h = getHeight();
	unsigned error = decodeBMP(image, w, h, bmp);

	if (error)
	{
		return false;
	}

	std::vector<unsigned char> png;
	error = lodepng::encode(png, image, w, h);

	if (error)
	{
		return false;
	}

	CST::Common::blob result(png.size());
	memcpy(result.mData, png.data(), result.mSize);

	return result;
}

////////////////////////////////////////////////////////////////////////////////////////

PNGImageSurface::PNGImageSurface(const size_t width, const size_t height)
{
	mWidth = width;
	mHeight = height;
	if (width * height)
	{
		mBuffer = (char*)CST_ALLOC(width * height * 4, "PNGImageSurface", "image");
		memset(mBuffer, 0, width * height * 4);
	}
	else
		mBuffer = NULL;
}

void PNGImageSurface::plot(const size_t x, const size_t y, const Color& color)
{
	if (color.isUndefined())
		return;
	if ((x >= mWidth) || (y == mHeight))
	{
		onErrorBoundaries(x, y);
		return;
	}
	if (!mBuffer)
		return;
	unsigned long index = (y * mWidth + x) * 4;
	ColorRGB col = color;
	unsigned char alpha = color.getAlpha();
	mBuffer[index + 0] = col.red;
	mBuffer[index + 1] = col.green;
	mBuffer[index + 2] = col.blue;
	mBuffer[index + 3] = alpha;
}

Color PNGImageSurface::peek(const size_t x, const size_t y)
{
	if (!mBuffer)
		return Color();
	if ((x >= mWidth) || (y >= mHeight))
	{
		onErrorBoundaries(x, y);
		return Color();
	}
	unsigned long index = (y * mWidth + x) * 4;
	ColorRGB col;
	unsigned char alpha;
	col.red = mBuffer[index + 0];
	col.green = mBuffer[index + 1];
	col.blue = mBuffer[index + 2];
	alpha = mBuffer[index + 3];
	return Color(col, alpha);
}

size_t PNGImageSurface::getWidth() const
{
	return mWidth;
}

size_t PNGImageSurface::getHeight() const
{
	return mHeight;
}

bool PNGImageSurface::read(const std::string& fname)
{
	reset();
	unsigned error = lodepng_decode32_file((unsigned char**)&mBuffer, &mWidth, &mHeight, fname.c_str());
	return error == 0;
}

bool PNGImageSurface::write(const std::string& fname)
{
	unsigned error = lodepng_encode32_file(fname.c_str(), (unsigned char*)mBuffer, mWidth, mHeight);
	return error == 0;
}

void PNGImageSurface::reset()
{
	CST_FREE(mBuffer);
	mBuffer = NULL;
}

size_t PNGImageSurface::getMemorySize() const
{
	return mWidth * mHeight * 4;
}
