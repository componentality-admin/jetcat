/*************************************************************************************/
/* Portable Graphics Library for Embedded Systems * (C) Componentality Oy, 2015-2016 */
/* Initial design and development: Konstantin A. Khait                               */
/* Support, comments and questions: dev@componentality.com                           */
/*************************************************************************************/

#include "rotator.h"

using namespace Componentality::Graphics::JetCat;
using namespace Componentality::Graphics;

Rotator::Rotator(const double angle, const Componentality::Graphics::Point center)
{
	mAngle = angle;
	mCenter = center;
}

Rotator:: ~Rotator()
{
}

Margins Rotator::draw(ISurface& surface)
{
	RotationPort rp(surface, mAngle, mCenter);
	return Container::draw(rp);
}
