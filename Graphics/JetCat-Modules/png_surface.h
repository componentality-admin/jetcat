/*************************************************************************************/
/* Portable Graphics Library for Embedded Systems * (C) Componentality Oy, 2015-2016 */
/* Initial design and development: Konstantin A. Khait                               */
/* Support, comments and questions: dev@componentality.com                           */
/*************************************************************************************/

#ifndef _PNG_SURFACE_H_
#define _PNG_SURFACE_H_

#include "../Surface/bmp_surface.h"

namespace Componentality
{
	namespace Graphics
	{

		// Redefinition of Bitmap surface for PNG read/write.
		// It keeps no-alpha 24-bit bitmap, but readable from PNG
		class PNGSurface : public BitmapSurface
		{
		public:
			PNGSurface(const size_t width = 0, const size_t height = 0) : BitmapSurface(width, height) {}; // Makes bitmap of the given size
			virtual ~PNGSurface() {};
		public:
			// Reading of the PNG from a file
			virtual bool read(const std::string&);
			// Writing of the PNG to a file
			virtual bool write(const std::string&);
			// Retrieve blob with PNG data
			virtual CST::Common::blob get();
		};

		// Pure PNG image with alpha channel
		class PNGImageSurface : public IImageSurface
		{
		protected:
			unsigned mWidth;
			unsigned mHeight;
		public:
			PNGImageSurface(const size_t width = 0, const size_t height = 0); // Makes bitmap of the given size
			virtual ~PNGImageSurface() {};
		public:
			// Surface-based set and get pixel's color methods
			virtual void plot(const size_t x, const size_t y, const Color&);
			virtual Color peek(const size_t x, const size_t y);
			// Get width
			virtual size_t getWidth() const;
			// Get height
			virtual size_t getHeight() const;
			// Reading of the bitmap from a file
			virtual bool read(const std::string&);
			// Writing of the bitmap to a file
			virtual bool write(const std::string&);
			// Clear memory and unload image
			virtual void reset();
			// Retrieve memory size
			virtual size_t getMemorySize() const;
		};

	}
}

#endif
