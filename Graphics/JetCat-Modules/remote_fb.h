/*************************************************************************************/
/* Portable Graphics Library for Embedded Systems * (C) Componentality Oy, 2015-2016 */
/* Initial design and development: Konstantin A. Khait                               */
/* Support, comments and questions: dev@componentality.com                           */
/*************************************************************************************/

#ifndef __REMOTE_FB_H__
#define __REMOTE_FB_H__

#include "../common-libs/HTTP/http_agent.h"
#include "../Surface/surface.h"
#include "../Surface/bmp_surface.h"
#include "../JetCat/drawable.h"
#include "../JetCat/window.h"

namespace Componentality
{
	namespace Graphics
	{

		class SurfaceAccessProvider : public Componentality::HTTP::HTTPResourceProvider
		{
		protected:
			Componentality::HTTP::HTTPServer& mServer;
		public:
			SurfaceAccessProvider(Componentality::HTTP::HTTPServer& server);
			virtual ~SurfaceAccessProvider();
		protected:
			virtual std::pair<std::string, std::string> getResource(const std::string& address, const std::vector<std::string>& headers);
			virtual ISurface* getSurface(PARSED_URL*) = 0;
			virtual uint32_t getChecksum(ISurface*);
		private:
			static std::pair<int, bool> retrieveArgument(PARSED_URL&, const std::string argname, const int default_value);
		};

		class StaticSurfaceAccessProvider : public SurfaceAccessProvider
		{
		protected:
			ISurface& mSurface;
		public:
			StaticSurfaceAccessProvider(Componentality::HTTP::HTTPServer& server, ISurface& surface) : SurfaceAccessProvider(server), mSurface(surface) {};
			virtual ~StaticSurfaceAccessProvider() {};
		protected:
			virtual ISurface* getSurface(PARSED_URL*) { return &mSurface; };
		};

		class FramebufAccessProvider : public SurfaceAccessProvider
		{
		protected:
			Componentality::Graphics::JetCat::WindowManager& mWindowManager;
		public:
			FramebufAccessProvider(Componentality::HTTP::HTTPServer& server, Componentality::Graphics::JetCat::WindowManager& wm) : SurfaceAccessProvider(server), mWindowManager(wm) {};
			virtual ~FramebufAccessProvider() {};
		protected:
			virtual ISurface* getSurface(PARSED_URL*) { return mWindowManager.getSurface(); };
		};

		class MouseAccessProvider : public Componentality::HTTP::HTTPResourceProvider
		{
		protected:
			Componentality::Graphics::JetCat::WindowManager& mWindowManager;
			Componentality::HTTP::HTTPServer& mServer;
		public:
			MouseAccessProvider(Componentality::HTTP::HTTPServer& server, Componentality::Graphics::JetCat::WindowManager& wm) : mServer(server), mWindowManager(wm)
			{
				mServer.subscribe(*this);
			};
			virtual ~MouseAccessProvider()
			{
				mServer.unsubscribe(*this);
			};
		protected:
			virtual std::pair<std::string, std::string> getResource(const std::string& address, const std::vector<std::string>& headers);
		};

		class HTTPCapturer : public SurfaceAccessProvider, public Componentality::Graphics::JetCat::IElement
		{
		protected:
			BitmapSurface* mSurface;
			CST::Common::mutex mLock;
		public:
			HTTPCapturer(Componentality::HTTP::HTTPServer& server);
			virtual ~HTTPCapturer();
		protected:
			virtual ISurface* getSurface(PARSED_URL*);
			virtual Componentality::Graphics::JetCat::Margins draw(ISurface&);
		};

		class FileSystemAccessProvider : public Componentality::HTTP::HTTPResourceProvider
		{
		protected:
			std::string mRootFolder;
			Componentality::HTTP::HTTPServer& mServer;
		public:
			FileSystemAccessProvider(Componentality::HTTP::HTTPServer& server, const std::string root = "");
			virtual ~FileSystemAccessProvider();
		protected:
			virtual std::pair<std::string, std::string> getResource(const std::string& address, const std::vector<std::string>& headers);
		};

		class FileAccessProvider : public FileSystemAccessProvider
		{
		protected:
			std::map<std::string, std::string> mMime;
		public:
			FileAccessProvider(Componentality::HTTP::HTTPServer& server, const std::string root = "");
			virtual ~FileAccessProvider();
		protected:
			virtual std::pair<std::string, std::string> getResource(const std::string& address, const std::vector<std::string>& headers);
		};

	} // namespace Graphics
} // namespace Componentality

#endif
