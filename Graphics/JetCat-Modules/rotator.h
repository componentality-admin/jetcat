/*************************************************************************************/
/* Portable Graphics Library for Embedded Systems * (C) Componentality Oy, 2015-2016 */
/* Initial design and development: Konstantin A. Khait                               */
/* Support, comments and questions: dev@componentality.com                           */
/*************************************************************************************/

#ifndef __ROTATOR_H__
#define __ROTATOR_H__

#include "../JetCat/placement.h"

namespace Componentality
{
	namespace Graphics
	{
		namespace JetCat
		{

			class Rotator : public Container
			{
			protected:
				Point mCenter;					// Rotation center coordinates
				double mAngle;					// Rotation angle
			public:
				Rotator(const double angle, const Componentality::Graphics::Point center = Componentality::Graphics::Point());
				virtual ~Rotator();
			protected:
				virtual Margins draw(ISurface&);
			};

		}
	} // namespace Graphics
} // namespace Componentality


#endif