/*************************************************************************************/
/* Portable Graphics Library for Embedded Systems * (C) Componentality Oy, 2015-2016 */
/* Initial design and development: Konstantin A. Khait                               */
/* Support, comments and questions: dev@componentality.com                           */
/*************************************************************************************/

#ifndef __TOUCH_H__
#define __TOUCH_H__

#include "../JetCat/inputs.h"
#include "../JetCat/drawable.h"
#include "../JetCat/placement.h"

#ifdef WIN32
#define DEFAULT_CALIBRATION_FILE "c:\\Componentality\\touch_calibrations.dat"
#else
#define DEFAULT_CALIBRATION_FILE "/var/componentality/touch_calibrations.dat"
#endif

namespace Componentality
{
	namespace Graphics
	{
		namespace JetCat
		{
			extern const std::string EVENT_TYPE_LONG_PRESS;		// Event generated upon long press
			extern const std::string EVENT_TYPE_SHORT_PRESS;	// Event generated upon short press
			extern const std::string EVENT_TYPE_EASY_GESTURE;	// Easy gesture event

			class LongPressEvent : public PressEvent
			{
			public:
				LongPressEvent() { mType = EVENT_TYPE_LONG_PRESS; }
				virtual ~LongPressEvent() {};
			};

			class ShortPressEvent : public PressEvent
			{
			public:
				ShortPressEvent() { mType = EVENT_TYPE_SHORT_PRESS; }
				virtual ~ShortPressEvent() {};
			};

			class EasyGestureEvent : public IInputEvent
			{
			public:
				enum DIRECTION
				{
					LEFT, RIGHT, UP, DOWN
				};
				struct Payload : public Event::Payload
				{
					DIRECTION mDirection;
					Point mStart;
					Point mEnd;
					Payload(Point start, Point end, DIRECTION dir) : mStart(start), mEnd(end), mDirection(dir) {};
					virtual ~Payload() {};
				};
			public:
				EasyGestureEvent(Point start, Point end, DIRECTION dir) 
				{ 
					mType = EVENT_TYPE_EASY_GESTURE; 
					setPayload(*CST_NEW(Payload(start, end, dir), "EasyGestureEvent", "payload"));
				}
				virtual ~EasyGestureEvent() 
				{
				};
				virtual DIRECTION getDirection() const { return ((Payload*)getPayload())->mDirection; }
				virtual Point getStart() const { return ((Payload*)getPayload())->mStart; }
				virtual Point getEnd() const { return ((Payload*)getPayload())->mEnd; }
			public:
				virtual bool filter(Componentality::Graphics::Point lefttop, Componentality::Graphics::Point rightbottom);
			};


			// Filter for separation of long and short presses
			class TouchEventsProcessor : public Container
			{
			protected:
				size_t mLongPressBound;						// Time in milliseconds exceeding which we consider as long press
				size_t mTicksAfterPressed;					// Number of 100ms ticks after press happened
				bool   mSourceEventTransfer;				// True if source press/depress events to be also transferred on long press
				Point  mPoint;								// Point where pressed
			public:
				TouchEventsProcessor(const size_t bound = 300, const bool process_source = true)
					: mLongPressBound(bound), mTicksAfterPressed(0), mSourceEventTransfer(process_source) {setEventFilter(CST_NEW(StdEventFilter, "Touch Event Processor", "event filter")); };
				virtual ~TouchEventsProcessor() {}
			protected:
				virtual bool onEvent(CST::Common::Event& event);
			};

			// Filter for separation of easy gestures
			class EasyGesturesProcessor : public TouchEventsProcessor
			{
			protected:
				size_t mBoundaryPercent;
				size_t mShiftBound;
				bool   mHorizontalAllowed;
				bool   mVerticalAllowed;
			public:
				EasyGesturesProcessor(const size_t boundary_percent = 25,	// Percent of orthogonal shift allowed to recognize a gesture
					const size_t shift_bound = 20,							// Minimum shift to recognize a gesture
					const bool horizontal_allowed = true,					// True if horizontal gestures allowed
					const bool vertical_allowed = true,						// Vertical gestures allowed
					const size_t long_press_bound = 300,					// Milliseconds to detect long pressure
					const bool process_source = false);						// Send press/depress messages while long/short presses recognition
				virtual ~EasyGesturesProcessor();
			protected:
				virtual bool onEvent(CST::Common::Event& event);
				virtual bool filter(CST::Common::Event& event);
			};

			class TouchCalibrator : public Container
			{
			public:
				struct CALIBRATIONS
				{
					double mKX;
					double mBX;
					double mKY;
					double mBY;
				};
			protected:
				CALIBRATIONS mCalibrations;
			public:
				TouchCalibrator(const double kx = 1.0, const double ky = 1.0, const double bx = 0.0, const double by = 0.0);
				TouchCalibrator(const CALIBRATIONS&);
				TouchCalibrator(const std::string& filename);
				virtual ~TouchCalibrator();
				virtual operator CALIBRATIONS() const { return mCalibrations; };
				virtual TouchCalibrator& operator=(const CALIBRATIONS&);
				virtual void set(const double kx, const double ky, const double bx, const double by);
				virtual bool write(const std::string& filename);
				virtual bool read(const std::string& filename);
			protected:
				virtual bool onEvent(CST::Common::Event& event);
			};

			class Calibrate : public TouchCalibrator
			{
			protected:
				Point* mLeftTop;
				Point* mRightTop;
				Point* mLeftBottom;
				Point* mRightBottom;
				size_t mWidth;
				size_t mHeight;
				bool   mCleaned;
				std::string mCalibrationFile;
				bool   mCalibrated;
			public:
				Calibrate(const std::string mCalibrationFile = DEFAULT_CALIBRATION_FILE);
				virtual ~Calibrate();
			protected:
				virtual Margins draw(ISurface&);
				virtual bool onEvent(CST::Common::Event& event);
				virtual void drawTarget(ISurface&, Point);
				virtual void clear(ISurface&);
				static void calibrate(double p1, double p2, double a1, double a2, double& k, double& b);
				void calibrate(Point& pressed1, Point& pressed2, Point drawn1, Point drawn2);
			};

		} // namespace JetCat
	} // namespace Graphics
} // namespace Componentality

#endif
