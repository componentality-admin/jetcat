/********************************************************************************/
/* Portable Graphics Library for Embedded Systems * (C) Componentality Oy, 2016 */
/* Initial design and development: Konstantin A. Khait                          */
/* Support, comments and questions: dev@componentality.com                      */
/********************************************************************************/
/* Layouts help arranging graphic elements on the screen without changing       */
/* of coordinates directly                                                      */
/********************************************************************************/

#include "layout.h"

using namespace Componentality::Graphics::JetCat;
using namespace Componentality::Graphics;

void ILayout::autosize(ISurface& surface)
{
	if (getSize().first == ____UNDEFINED)
		this->size(surface.getWidth(), getSize().second);
	if (getSize().second == ____UNDEFINED)
		this->size(getSize().first, surface.getHeight());
}

void ILinearLayout::add(const Tag tag, IElement& element)
{
}

void ILinearLayout::add(const Tag tag, Placement& placement, const size_t shift, const size_t padding)
{
	PLACEMENT _placement;
	_placement.mPlacement = &placement;
	_placement.mShift = shift;
	_placement.mPadding = padding;
	_placement.mOriginalHeight = _placement.mOriginalWidth = ____UNDEFINED;
	mPlacements.push_back(_placement);
	ILayout::add(tag, placement);
}

void ILinearLayout::add(const Tag tag, Placement& placement, const double weight, const size_t padding)
{
	PLACEMENT _placement;
	_placement.mPlacement = &placement;
	_placement.mWeight = weight;
	_placement.mPadding = padding;
	_placement.mOriginalHeight = _placement.mOriginalWidth = ____UNDEFINED;
	mPlacements.push_back(_placement);
	ILayout::add(tag, placement);
}

Margins VerticalLayout::draw(ISurface& surface)
{
	autosize(surface);

	int pos = 0;
	int hsize = 0;
	bool noadjust = false;
	for (std::list<PLACEMENT>::iterator i = mPlacements.begin(); i != mPlacements.end(); i++)
	{
		i->mPlacement->move(0, pos);
		if (i->mShift != ____UNDEFINED)
		{
			pos += i->mShift;
		}
		else
		{
			pos += i->mPadding;
			if (i->mPlacement->getSize().second != ____UNDEFINED)
				pos += i->mPlacement->getSize().second;
		}
		if (mStretch)
		{
			i->mOriginalWidth = i->mPlacement->getSize().first;
			i->mPlacement->size(getSize().first, i->mPlacement->getSize().second);
		}
		else
		{
			if (i->mOriginalWidth != ____UNDEFINED)
				i->mPlacement->size(i->mOriginalWidth, i->mPlacement->getSize().second);
		}
		hsize = ____max(hsize, (int) i->mPlacement->getSize().first);
	}
	int stretch_size = (mGravity != CENTER) ? getSize().second - pos : (getSize().second - pos) / 2;
	if ((mGravity == TOP) || (mGravity == CENTER))
	{
		if (mPlacements.begin() != mPlacements.end())
		{
			// Resize last element if it has flexible size
			std::list<PLACEMENT>::iterator end = mPlacements.end();
			end--;
			if (end->mPlacement->getSize().second == ____UNDEFINED)
			{
				end->mPlacement->size(end->mPlacement->getSize().first, stretch_size);
				noadjust = true;
			}
		}
	}
	if ((mGravity == TOP) || (mGravity == CENTER))
	{
		if (mPlacements.begin() != mPlacements.end())
		{
			if (mPlacements.begin()->mPlacement->getSize().second == ____UNDEFINED)
			{
				mPlacements.begin()->mPlacement->move(mPlacements.begin()->mPlacement->getPosition().x, 0);
				mPlacements.begin()->mPlacement->size(mPlacements.begin()->mPlacement->getSize().first, stretch_size);
				for (std::list<PLACEMENT>::iterator i = mPlacements.begin(); ;)
				{
					i++;
					if (i == mPlacements.end()) break;
					i->mPlacement->move(i->mPlacement->getPosition().x, i->mPlacement->getPosition().y + stretch_size);
				}
				noadjust = true;
			}
		}
		if (!noadjust)
			move(getPosition().x, stretch_size);
	}
	if (mWAdjust)
		size((size_t) hsize, getSize().second);
	if (mHAdjust && !noadjust)
		size(getSize().first, pos);
	return ILinearLayout::draw(surface);
}

Margins HorizontalLayout::draw(ISurface& surface)
{
	autosize(surface);

	int pos = 0;
	int vsize = 0;
	bool noadjust = false;
	for (std::list<PLACEMENT>::iterator i = mPlacements.begin(); i != mPlacements.end(); i++)
	{
		i->mPlacement->move(pos, 0);
		if (i->mShift != ____UNDEFINED)
		{
			pos += i->mShift;
		}
		else
		{
			pos += i->mPadding;
			if (i->mPlacement->getSize().first != ____UNDEFINED)
				pos += i->mPlacement->getSize().first;
		}
		if (mStretch)
		{
			i->mOriginalHeight = i->mPlacement->getSize().second;
			i->mPlacement->size(i->mPlacement->getSize().first, getSize().second);
		}
		else
		{
			if (i->mOriginalHeight != ____UNDEFINED)
				i->mPlacement->size(i->mPlacement->getSize().first, i->mOriginalHeight);
		}
		vsize = ____max(vsize, (int) i->mPlacement->getSize().second);
	}
	int stretch_size = (mGravity != CENTER) ? getSize().first - pos : (getSize().first - pos) / 2;
	if ((mGravity == LEFT) || (mGravity == CENTER))
	{
		if (mPlacements.begin() != mPlacements.end())
		{
			// Resize last element if it has flexible size
			std::list<PLACEMENT>::iterator end = mPlacements.end();
			end--;
			if (end->mPlacement->getSize().first == ____UNDEFINED)
			{
				end->mPlacement->size(stretch_size, end->mPlacement->getSize().second);
				noadjust = true;
			}
		}
	}
	if ((mGravity == RIGHT) || (mGravity == CENTER))
	{
		if (mPlacements.begin() != mPlacements.end())
		{
			if (mPlacements.begin()->mPlacement->getSize().first == ____UNDEFINED)
			{
				mPlacements.begin()->mPlacement->move(0, mPlacements.begin()->mPlacement->getPosition().y);
				mPlacements.begin()->mPlacement->size(stretch_size, mPlacements.begin()->mPlacement->getSize().second);
				for (std::list<PLACEMENT>::iterator i = mPlacements.begin(); ;)
				{
					i++;
					if (i == mPlacements.end()) break;
					i->mPlacement->move(i->mPlacement->getPosition().x + stretch_size, i->mPlacement->getPosition().y);
				}
				noadjust = true;
			}
		}
		if (!noadjust)
			move(stretch_size, getPosition().y);
	}
	if (mHAdjust)
		size(getSize().first, (size_t)vsize);
	if (mWAdjust && !noadjust)
		size(pos, getSize().second);
	return ILinearLayout::draw(surface);
}

Margins ProportionalVerticalLayout::draw(ISurface& surface)
{
	autosize(surface);

	int pos = 0;
	int hsize = 0;
	for (std::list<PLACEMENT>::iterator i = mPlacements.begin(); i != mPlacements.end(); i++)
	{
		i->mPlacement->move(0, pos);
		if (i->mWeight > 0.0)  // Recalculate item height according to it's weight
		{
			double height = i->mWeight / mTotalWeight * (getSize().second - mTotalFixedSize);
			i->mPlacement->size(i->mPlacement->getSize().first, (size_t)height);
		}
		if (i->mShift != ____UNDEFINED)
		{
			pos += i->mShift;
		}
		else
		{
			pos += i->mPadding;
			if (i->mPlacement->getSize().second != ____UNDEFINED)
				pos += i->mPlacement->getSize().second;
		}
		if (mStretch)
		{
			i->mOriginalWidth = i->mPlacement->getSize().first;
			i->mPlacement->size(getSize().first, i->mPlacement->getSize().second);
		}
		else
		{
			if (i->mOriginalWidth != ____UNDEFINED)
				i->mPlacement->size(i->mOriginalWidth, i->mPlacement->getSize().second);
		}
		hsize = ____max(hsize, (int)i->mPlacement->getSize().first);
	}
	if (mWAdjust)
		size((size_t)hsize, getSize().second);
	if (mHAdjust)
		size(getSize().first, pos);
	return ILinearLayout::draw(surface);
}

void ProportionalVerticalLayout::add(const Tag tag, Placement& placement, const size_t padding)
{
	if (placement.getSize().second != ____UNDEFINED)
		mTotalFixedSize += placement.getSize().second;
	mTotalFixedSize += padding;
	ILinearLayout::add(tag, placement, ____UNDEFINED, padding);
}

void ProportionalVerticalLayout::add(const Tag tag, Placement& placement, const double weight, const size_t padding)
{
	mTotalWeight += weight;
	if (placement.getSize().second != ____UNDEFINED)
		mTotalFixedSize += placement.getSize().second;
	mTotalFixedSize += padding;
	ILinearLayout::add(tag, placement, weight, padding);
}

Margins ProportionalHorizontalLayout::draw(ISurface& surface)
{
	autosize(surface);

	int pos = 0;
	int vsize = 0;
	for (std::list<PLACEMENT>::iterator i = mPlacements.begin(); i != mPlacements.end(); i++)
	{
		i->mPlacement->move(pos, 0);
		if (i->mWeight > 0.0)  // Recalculate item height according to it's weight
		{
			double width = i->mWeight / mTotalWeight * (getSize().first  - mTotalFixedSize);
			i->mPlacement->size((size_t) width, i->mPlacement->getSize().second);
		}
		if (i->mShift != ____UNDEFINED)
		{
			pos += i->mShift;
		}
		else
		{
			pos += i->mPadding;
			if (i->mPlacement->getSize().first != ____UNDEFINED)
				pos += i->mPlacement->getSize().first;
		}
		if (mStretch)
		{
			i->mOriginalHeight = i->mPlacement->getSize().second;
			i->mPlacement->size(i->mPlacement->getSize().first, getSize().second);
		}
		else
		{
			if (i->mOriginalHeight != ____UNDEFINED)
				i->mPlacement->size(i->mPlacement->getSize().first, i->mOriginalHeight);
		}
		vsize = ____max((int)i->mPlacement->getSize().second, vsize);
	}
	if (mHAdjust)
		size(getSize().first, (size_t)vsize);
	if (mWAdjust)
		size(pos, getSize().second);
	return ILinearLayout::draw(surface);
}

void ProportionalHorizontalLayout::add(const Tag tag, Placement& placement, const size_t padding)
{
	if (placement.getSize().first != ____UNDEFINED)
		mTotalFixedSize += placement.getSize().first;
	mTotalFixedSize += padding;
	ILinearLayout::add(tag, placement, ____UNDEFINED, padding);
}

void ProportionalHorizontalLayout::add(const Tag tag, Placement& placement, const double weight, const size_t padding)
{
	mTotalWeight += weight;
	if (placement.getSize().first != ____UNDEFINED)
		mTotalFixedSize += placement.getSize().first;
	mTotalFixedSize += padding;
	ILinearLayout::add(tag, placement, weight, padding);
}

Margins FrameLayout::draw(ISurface& surface)
{
	autosize(surface);

	if (!mObject)
		return Margins();
	if (mWAdjust && (mObject->getSize().first != ____UNDEFINED))
		size(mObject->getSize().first + mMargins.mLeft + mMargins.mRight, getSize().second);
	if (mHAdjust && (mObject->getSize().second != ____UNDEFINED))
		size(getSize().first, mObject->getSize().second + mMargins.mTop + mMargins.mBottom);
	Point pos = Point(mMargins.mLeft, mMargins.mTop) + mPosition;
	mObject->move(pos.x, pos.y);
	if (mStretch)
	{
		mObject->size(getSize().first - mMargins.mLeft - mMargins.mRight - 1, getSize().second - mMargins.mTop - mMargins.mBottom - 1);
	}
	return IElement::draw(surface);
}

//////////////////////////////////////////////////////////////////////////////////////////////////

GridLayout::GridLayout(const size_t hcells, const size_t vcells, const size_t cell_width, const size_t cell_height, const bool stretch)
	: mHCells(hcells), mVCells(vcells), mCellWidth(cell_width), mCellHeight(cell_height), ILayout(stretch, false, false)
{
}

GridLayout::~GridLayout()
{
}

void GridLayout::add(const size_t x, const size_t y, Placement& element)
{
	mObjects[std::pair<size_t, size_t>(x, y)] = &element;
	std::string tag;
	char buf[20];
	sprintf(buf, "%d", x);
	tag.append(buf);
	tag += ',';
	sprintf(buf, "%d", y);
	tag.append(buf);

	IElement::remove(tag);
	IElement::add(tag, element);
}

Margins GridLayout::draw(ISurface& surface)
{
	autosize(surface);

	ViewPort vp(surface, Point(getPosition().x, getPosition().y), Point(getSize().first + getPosition().x, getSize().second + getPosition().y));

	if (mCellWidth == ____UNDEFINED)
		mCellWidth = vp.getWidth() / mHCells;
	if (mCellHeight == ____UNDEFINED)
		mCellHeight = vp.getHeight() / mVCells;

	for (size_t i = 0; i < mHCells; i++)
		for (size_t j = 0; j < mVCells; j++)
		{
			if (mObjects.find(std::pair<size_t, size_t>(i, j)) != mObjects.end())
			{
				Placement* item = mObjects[std::pair<size_t, size_t>(i, j)];
				if (!item)
					continue;
				if (mStretch)
					item->size(mCellWidth, mCellHeight);
				if (item->getSize().first == ____UNDEFINED)
					item->size(mCellWidth, item->getSize().second);
				if (item->getSize().second == ____UNDEFINED)
					item->size(item->getSize().first, mCellHeight);
				item->move(mCellWidth * i, mCellHeight * j);
			}
		}
	return ILayout::draw(surface);
}

//////////////////////////////////////////////////////////////////////////////////////

Margins AutoSizeLayout::draw(ISurface& surface)
{
	this->move(0, 0);
	this->size(surface.getWidth(), surface.getHeight());
	return FrameLayout::draw(surface);
}
