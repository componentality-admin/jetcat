/*************************************************************************************/
/* Portable Graphics Library for Embedded Systems * (C) Componentality Oy, 2015-2016 */
/* Initial design and development: Konstantin A. Khait                               */
/* Support, comments and questions: dev@componentality.com                           */
/*************************************************************************************/

#include "remote_fb.h"
#include "png_surface.h"
#include "../JetCat/inputs.h"
#include "../common-libs/common/common_utilities.h"
#include "../common-libs/common/crc32.h"

#ifdef WIN32
const std::string TEMPORARY_FOLDER = "C:\\Componentality\\Temporary";
const std::string HTTP_ROOT_FOLDER = "C:\\Componentality\\HTTP";
#else
const std::string HTTP_ROOT_FOLDER = "/var/componentality/http";
const std::string TEMPORARY_FOLDER = "/tmp";
#endif

using namespace Componentality::HTTP;

struct EXT_MIME_DESCRIPTION
{
	std::string mExtension;
	std::string mMime;
};

EXT_MIME_DESCRIPTION __ext_mime_mapping[] =
{
	{ "json", MIME_APP_JSON },
	{ "pdf", MIME_APP_PDF },
	{ "ps", MIME_APP_POSTSCRIPT },
	{ "zip", MIME_APP_ZIP },
	{ "gz", MIME_APP_GZIP },
	{ "pcm", MIME_AUDIO_BASIC },
	{ "aac", MIME_AUDIO_AAC },
	{ "mp3", MIME_AUDIO_MPEG },
	{ "wmv", MIME_AUDIO_WMV },
	{ "gif", MIME_IMAGE_GIF },
	{ "jpg", MIME_IMAGE_JPEG },
	{ "jpeg", MIME_IMAGE_JPEG },
	{ "png", MIME_IMAGE_PNG },
	{ "bmp", MIME_IMAGE_BMP },
	{ "svg", MIME_IMAGE_SVG },
	{ "tiff", MIME_IMAGE_TIFF },
	{ "ico", MIME_IMAGE_ICO },
	{ "webp", MIME_IMAGE_WEBP },
	{ "css", MIME_TEXT_CSS },
	{ "csv", MIME_TEXT_CSV },
	{ "htm", MIME_TEXT_HTML },
	{ "html", MIME_TEXT_HTML },
	{ "js", MIME_TEXT_JAVASCRIPT },
	{ "xml", MIME_TEXT_XML },
	{ "txt", MIME_TEXT_PLAIN },
	{ "mpeg", MIME_VIDEO_MPEG1 },
	{ "mpg", MIME_VIDEO_MPEG4 },
	{ "mp4", MIME_VIDEO_MPEG4 },
	{ "ogg", MIME_VIDEO_OGG },
	{ "mov", MIME_VIDEO_QUICKTIME },
	{ "webm", MIME_VIDEO_WEBM },
	{ "wmv", MIME_VIDEO_WMV },
	{ "flv", MIME_VIDEO_FLV },
	{ "3gpp", MIME_VIDEO_3GPP },
	{ "", ""}
};

const std::string UI_ACCESS_URL = "/ui";
const std::string UI_ACCESS_CHECKSUM_URL = "/ui_checksum";
const std::string UI_MOUSE_DOWN_URL = "/mouse_down";
const std::string UI_MOUSE_UP_URL = "/mouse_up";
const std::string UI_TOUCH_DOWN_URL = "/touch_down";
const std::string UI_TOUCH_UP_URL = "/touch_up";
const std::string UI_MOUSE_PARAM_X = "x";
const std::string UI_MOUSE_PARAM_Y = "y";

using namespace Componentality::Graphics;
using namespace Componentality::HTTP;

SurfaceAccessProvider::SurfaceAccessProvider(HTTPServer& server) : mServer(server)
{
	mServer.subscribe(*this);
}

SurfaceAccessProvider::~SurfaceAccessProvider() 
{
	mServer.unsubscribe(*this);
};

std::pair<int, bool> SurfaceAccessProvider::retrieveArgument(PARSED_URL& url, const std::string argname, const int default_value)
{
	std::pair<int, bool> result;
	result.first = default_value;
	result.second = false;
	if (url.second.find(argname) != url.second.end())
	{
		sscanf(url.second[argname].c_str(), "%d", &result.first);
		result.second = true;
	}
	return result;
}

std::pair<std::string, std::string> SurfaceAccessProvider::getResource(const std::string& address, const std::vector<std::string>& headers)
{
	PARSED_URL url_info = parseAddress(address);

	std::pair<std::string, std::string> result;

	ISurface* surface = getSurface(&url_info);
	bool surface_needs_removal = false;

	if (!surface)
		return result;
	std::pair<int, bool> x = retrieveArgument(url_info, "x", 0);
	std::pair<int, bool> y = retrieveArgument(url_info, "y", 0);
	std::pair<int, bool> width = retrieveArgument(url_info, "width", surface->getWidth());
	std::pair<int, bool> height = retrieveArgument(url_info, "height", surface->getHeight());
	if (x.second || y.second || width.second || height.second)
	{
		surface = CST_NEW(ViewPort(*surface, Point(x.first, y.first), Point(x.first, y.first) + Point(width.first - 1, height.first - 1)), "Surface Access Provider", "view port");
		surface_needs_removal = true;
	};

	if (url_info.first == UI_ACCESS_CHECKSUM_URL)
	{
		uint32_t checksum = getChecksum(surface);
		char buf[20];
		sprintf(buf, "%lu", checksum);
		result.first = buf;
		result.second = MIME_TEXT_PLAIN;
		if (surface_needs_removal)
			CST_DELETE(ViewPort, surface);
		return result;
	}

	if (url_info.first != UI_ACCESS_URL)
	{
		if (surface_needs_removal)
			CST_DELETE(ViewPort, surface);
		return result;
	}

	bool is_bitmap = false;			// If true, return result in BMP, otherwise in PNG
	if (url_info.second.find("format") != url_info.second.end())
		if (url_info.second["format"] == "bitmap")
			is_bitmap = true;

	if (!is_bitmap)
	{
		PNGSurface png(surface->getWidth(), surface->getHeight());
		png.apply(*surface);
		CST::Common::blob png_content = png.get();
		result.first = CST::Common::blobToString(png_content);
		png_content.purge();
		result.second = MIME_IMAGE_PNG;
	}
	else
	{
		BitmapSurface bmp(surface->getWidth(), surface->getHeight());
		bmp.apply(*surface);
		result.first.assign(bmp.getMemory(), bmp.getMemorySize());
		result.second = MIME_IMAGE_BMP;
	}
	if (surface_needs_removal)
		CST_DELETE(ViewPort, surface);
	return result;
}

uint32_t SurfaceAccessProvider::getChecksum(ISurface* surface)
{
	BitmapSurface bmp(surface->getWidth(), surface->getHeight());
	bmp.apply(*surface);
	return crc32(0, bmp.getMemory(), bmp.getMemorySize());
}

////////////////////////////////////////////////////////////////////////////////////////////

HTTPCapturer::HTTPCapturer(Componentality::HTTP::HTTPServer& server) : SurfaceAccessProvider(server)
{
	mSurface = NULL;
}

HTTPCapturer::~HTTPCapturer()
{
}

ISurface* HTTPCapturer::getSurface(PARSED_URL*)
{
	CST::Common::scoped_lock lock(mLock);
	return mSurface;
}

Componentality::Graphics::JetCat::Margins HTTPCapturer::draw(ISurface& surface)
{
	CST::Common::scoped_lock lock(mLock);
	JetCat::Margins margins = IElement::draw(surface);
	if (mSurface)
		CST_DELETE(ISurface, mSurface);
	mSurface = CST_NEW(BitmapSurface(surface.getWidth(), surface.getHeight()), "HTTPCapturer", "graphics buffer");
	mSurface->apply(surface);
	return margins;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::pair<std::string, std::string> MouseAccessProvider::getResource(const std::string& address, const std::vector<std::string>& headers)
{
	enum COMMAND
	{
		MOUSE_DOWN,
		MOUSE_UP,
		UNKNOWN
	} command = UNKNOWN;

	PARSED_URL url_info = parseAddress(address);
	int x = -1, y = -1;

	std::pair<std::string, std::string> result;

	if (url_info.first == UI_MOUSE_DOWN_URL)
		command = MOUSE_DOWN;
	if (url_info.first == UI_MOUSE_UP_URL)
		command = MOUSE_UP;

	if (command == UNKNOWN)
		return result;

	result.first = "ERROR: ";

	if (url_info.second.find(UI_MOUSE_PARAM_X) != url_info.second.end())
		sscanf(url_info.second[UI_MOUSE_PARAM_X].c_str(), "%d", &x);
	else
		result.first += "X parameter not provided; ";

	if (url_info.second.find(UI_MOUSE_PARAM_Y) != url_info.second.end())
		sscanf(url_info.second[UI_MOUSE_PARAM_Y].c_str(), "%d", &y);
	else
		result.first += "Y parameter not provided; ";

	if ((x > 0) && (y > 0))
	{
		Componentality::Graphics::JetCat::IInputEvent* event;
		if (command == MOUSE_DOWN)
			event = CST_NEW(Componentality::Graphics::JetCat::PressEvent((size_t)x, (size_t)y), "MouseAccessProvider", "mouse down event");
		if (command == MOUSE_UP)
			event = CST_NEW(Componentality::Graphics::JetCat::DepressEvent((size_t)x, (size_t)y), "MouseAccessProvider", "mouse up event");
		mWindowManager.sendEvent(*event);
		result.first = "OK";
	}
	result.second = MIME_TEXT_PLAIN;
	return result;
}

//////////////////////////////////////////////////////////////////////////////////////////

FileSystemAccessProvider::FileSystemAccessProvider(Componentality::HTTP::HTTPServer& server, const std::string root) : mServer(server), mRootFolder(root)
{
	if (mRootFolder.empty())
		mRootFolder = HTTP_ROOT_FOLDER;
	mServer.subscribe(*this);
}

FileSystemAccessProvider::~FileSystemAccessProvider()
{
	mServer.unsubscribe(*this);
}

std::pair<std::string, std::string> FileSystemAccessProvider::getResource(const std::string& address, const std::vector<std::string>& headers)
{
	std::pair<std::string, std::string> result;
	std::string _address = address;
	if ((_address.size() > 0) && (_address[0] == '/'))
		_address.erase(0, 1);
	std::string fname = CST::Common::fileJoinPaths(mRootFolder, _address);
	if (!CST::Common::fileExists(fname))
		return result;
	CST::Common::blob file = CST::Common::fileRead(fname);
	if (file.mSize)
	{
		result.first = CST::Common::blobToString(file);
		result.second = MIME_APP_BIN;
	}
	file.purge();
	return result;
}

/////////////////////////////////////////////////////////////////////////////////////////

FileAccessProvider::FileAccessProvider(Componentality::HTTP::HTTPServer& server, const std::string root) : FileSystemAccessProvider(server, root)
{
	size_t i = 0;
	while (!__ext_mime_mapping[i].mMime.empty())
	{
		mMime[__ext_mime_mapping[i].mExtension] = __ext_mime_mapping[i].mMime;
		i++;
	}
}

FileAccessProvider::~FileAccessProvider()
{

}

std::pair<std::string, std::string> FileAccessProvider::getResource(const std::string& address, const std::vector<std::string>& headers)
{
	std::pair<std::string, std::string> result = FileSystemAccessProvider::getResource(address, headers);
	if (!result.second.empty())
	{
		size_t pos = ____UNDEFINED;
		for (size_t i = address.size(); i > 0; i--)
			if (address[i - 1] == '.')
			{
				pos = i - 1;
				break;
			}
		if (pos != ____UNDEFINED)
		{
			std::string ext = address.substr(pos + 1);
			if (mMime.find(ext) != mMime.end())
				result.second = mMime[ext];
		}
	}
	return result;
}
