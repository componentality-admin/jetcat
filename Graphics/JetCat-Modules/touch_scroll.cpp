/*************************************************************************************/
/* Portable Graphics Library for Embedded Systems * (C) Componentality Oy, 2015-2016 */
/* Initial design and development: Konstantin A. Khait                               */
/* Support, comments and questions: dev@componentality.com                           */
/*************************************************************************************/

#include "touch_scroll.h"

using namespace Componentality::Graphics;
using namespace Componentality::Graphics::JetCat;
using namespace CST::Common;

TouchMenu::TouchMenu() : mMenuEvent(NULL)
{
	mPressReceived = false;
	mGestureReceived = false;
	mMenuEventReceived = false;
	mIssueMenuEvent = false;
};

bool TouchMenu::onEvent(CST::Common::Event& event)
{
	Scroller::onEvent(event);
	EasyGesturesProcessor::onEvent(event);
	return true;
}

bool TouchMenu::preProcessEvent(CST::Common::Event& event)
{
	if (event.getType() == EVENT_TYPE_PRESS)
	{
		PressEvent& evt = (PressEvent&)event;
		if (!mPressReceived)
		{
			mPressPoint = evt.get();
			mPressReceived = true;
			mGestureReceived = false;
			return false;
		}
		else
		{
			mPressReceived = false;
			return true;
		}
	}
	else if (event.getType() == EVENT_TYPE_EASY_GESTURE)
	{
		mGestureReceived = true;
	}
	else if (event.getType() == EVENT_TYPE_MENU)
	{
		mMenuEvent.setItem(((MenuEvent&)event).getItem());
		if (!mMenuEventReceived)
		{
			mMenuEventReceived = true;
			return false;
		}
		else
		{
			return mIssueMenuEvent;
		}
	}
	else if (event.getType() == EVENT_TYPE_DEPRESS)
	{
		if (mMenuEventReceived && !mGestureReceived)
		{
			mIssueMenuEvent = true;
			onEvent(mMenuEvent);
			mMenuEventReceived = false;
			mIssueMenuEvent = false;
		}
		else
			mMenuEventReceived = false;
		if (mPressReceived && !mGestureReceived)
		{
			PressEvent evt(mPressPoint);
			onEvent(evt);
			mPressReceived = false;
			return false;
		}
		else
		{
			mPressReceived = false;
			mGestureReceived = false;
			if (!mGestureReceived)
				return true;
		}
	}
	return true;
}

bool VerticalTouchMenu::onEvent(CST::Common::Event& event)
{
	if (!VerticalMenu::filter(event))
		return false;
	if (event.getType() == EVENT_TYPE_EASY_GESTURE)
	{
		EasyGestureEvent& evt = (EasyGestureEvent&)event;
		if (evt.getDirection() == EasyGestureEvent::DOWN)
		{
			int scroll_to = evt.getEnd().y - evt.getStart().y;
			int shift = getScroll().second;
			if ((-shift >= scroll_to) || (scroll_to < 0))
				scroll_to = shift + scroll_to;
			else
				scroll_to = 0;
			scroll(0, scroll_to);
			VerticalMenu::raise(*CST_NEW(DrawRequestEvent((VerticalMenu*)this), "IMenu", "redraw"));
			preProcessEvent(event);
			return true;
		}
		if (evt.getDirection() == EasyGestureEvent::UP)
		{
			int scroll_to = evt.getStart().y - evt.getEnd().y;
			int maxscroll = mVerticalSize > mHeight ? mVerticalSize - mHeight : 0;
			int shift = getScroll().second;
			if (-shift + scroll_to <= maxscroll)
				scroll_to = - (shift + scroll_to);
			else
				scroll_to = - maxscroll;
			scroll(0, scroll_to);
			VerticalMenu::raise(*CST_NEW(DrawRequestEvent((VerticalMenu*)this), "IMenu", "redraw"));
			preProcessEvent(event);
			return true;
		}
	}
	TouchMenu::onEvent(event);
	if (preProcessEvent(event))
		VerticalMenu::onEvent(event); 
	return true;
}

bool HorizontalTouchMenu::onEvent(CST::Common::Event& event)
{
	if (!HorizontalMenu::filter(event))
		return false;
	if (event.getType() == EVENT_TYPE_EASY_GESTURE)
	{
		EasyGestureEvent& evt = (EasyGestureEvent&)event;
		if (evt.getDirection() == EasyGestureEvent::RIGHT)
		{
			int scroll_to = evt.getEnd().x - evt.getStart().x;
			int shift = getScroll().first;
			if ((-shift >= scroll_to) || (scroll_to < 0))
				scroll_to = shift + scroll_to;
			else
				scroll_to = 0;
			scroll(scroll_to, 0);
			HorizontalMenu::raise(*CST_NEW(DrawRequestEvent((HorizontalMenu*)this), "IMenu", "redraw"));
			preProcessEvent(event);
			return true;
		}
		if (evt.getDirection() == EasyGestureEvent::LEFT)
		{
			int scroll_to = evt.getStart().x - evt.getEnd().x;
			int maxscroll = mHorizontalSize > mWidth ? mHorizontalSize - mWidth : 0;
			int shift = getScroll().first;
			if (-shift + scroll_to <= maxscroll)
				scroll_to = -(shift + scroll_to);
			else
				scroll_to = -maxscroll;
			scroll(scroll_to, 0);
			HorizontalMenu::raise(*CST_NEW(DrawRequestEvent((HorizontalMenu*)this), "IMenu", "redraw"));
			preProcessEvent(event);
			return true;
		}
	}
	TouchMenu::onEvent(event);
	if (preProcessEvent(event))
		HorizontalMenu::onEvent(event);
	return true;
}
