/*************************************************************************************/
/* Portable Graphics Library for Embedded Systems * (C) Componentality Oy, 2015-2016 */
/* Initial design and development: Konstantin A. Khait                               */
/* Support, comments and questions: dev@componentality.com                           */
/*************************************************************************************/

#ifndef __IMAGEBOX_H__
#define __IMAGEBOX_H__

#include "global.h"
#include "drawable.h"

namespace Componentality
{
	namespace Graphics
	{
		namespace JetCat
		{
			class Image : public IElement
			{
			protected:
				std::string mFilename;
				IImageSurface* mImage;		// Cached image
				bool mRead;					// Image is already cached
			public:
				Image();
				Image(const std::string file) :
					mFilename(file) { mRead = false; mImage = NULL; };
				virtual ~Image();
				virtual std::string getFilename() const { return mFilename; }
				virtual void setFilename(const std::string file) { if (mFilename != file) { mFilename = file; mRead = false; clear(); }; };
				virtual void clear() { if (mImage) { mImage->reset(); CST_DELETE(IImageSurface, mImage); mImage = NULL; } mRead = false; };
				virtual void setImage(IImageSurface& image, const bool read = false) { clear(); mImage = &image; mRead = read; }
			protected:
				virtual void readToCache();
			protected:
				virtual Margins draw(ISurface&);
			};

			class ImageBox : public Image, public ISizeable
			{
			protected:
				struct COMPARISON_RESULT
				{
					unsigned char rating;	// Rating of comparison. Higher rating means more trustability
					size_t difference;		// Difference value
				};
			protected:
				std::list<std::string> mImageFiles;
				bool mKeepProportions;
			public:
				ImageBox(const size_t width = ____UNDEFINED, const size_t height = ____UNDEFINED, const bool proportional = false);
				ImageBox(const std::string file, const size_t width = ____UNDEFINED, const size_t height = ____UNDEFINED, const bool proportional = false);
				ImageBox(const std::string files[], const size_t width = ____UNDEFINED, const size_t height = ____UNDEFINED, const bool proportional = false);
				ImageBox(const std::list<std::string> files, const size_t width = ____UNDEFINED, const size_t height = ____UNDEFINED, const bool proportional = false);
				virtual ~ImageBox();
				virtual void size(const size_t width, const size_t height);
				void setImage(IImageSurface& image, const bool read = false) { if (mImageFiles.empty()) { Image::setImage(image, read); mWidth = image.getWidth(); mHeight = image.getHeight(); }; }
			protected:
				virtual Margins draw(ISurface&);
				virtual COMPARISON_RESULT getDifference(BitmapSurface& to_compare);
				virtual void readToCache();
			};


		} // namespace JetCat
	} // namespace Graphics
} // namespace Componentality

#endif