/*************************************************************************************/
/* Portable Graphics Library for Embedded Systems * (C) Componentality Oy, 2015-2016 */
/* Initial design and development: Konstantin A. Khait                               */
/* Support, comments and questions: dev@componentality.com                           */
/*************************************************************************************/

#include "placement.h"
#include "Drawing/viewport.h"

using namespace Componentality::Graphics;
using namespace Componentality::Graphics::JetCat;

Placement::Placement()
{
	mWidth = mHeight = ____UNDEFINED;
};

Placement::Placement(const size_t x, const size_t y, const size_t width, const size_t height)
{
	move(x, y);
	size(width, height);
}

Placement::~Placement()
{
}

Margins Placement::draw(ISurface& surface)
{
	if (isHidden())
		return Margins();
	if ((mWidth != ____UNDEFINED) && (mHeight != ____UNDEFINED))
	{
		Point bottomright = (mWidth != ____UNDEFINED) && (mHeight != ____UNDEFINED) ? Point(mPosition.x + mWidth - 1, mPosition.y + mHeight - 1) : IElement::getRightBottom();
		ViewPort vp(surface, mPosition, bottomright);
		mTopLeft.x += mPosition.x; mTopLeft.y += mPosition.y;
		mRightBottom.x = mTopLeft.x + mWidth - 1; mRightBottom.y = mTopLeft.y + mHeight - 1;
		setTopLeft(mTopLeft);
		setRightBottom(mRightBottom);
		IElement::draw(vp);
	}
	else
	{
		MovingPort mp(surface, mPosition);
		mTopLeft.x += mPosition.x; mTopLeft.y += mPosition.y;
		mRightBottom = Point(surface.getWidth(), surface.getHeight()) - mPosition + mTopLeft;
		IElement::draw(mp);
	}
	return Margins();
}

///////////////////////////////////////////////////////////////////////////////////////////

FastPlacement::FastPlacement()
{
}

FastPlacement::~FastPlacement()
{
}

Margins FastPlacement::draw(ISurface& surface)
{
	if (isHidden())
		return Margins();

	ISurface* dst;
	if ((mWidth != ____UNDEFINED) && (mHeight != ____UNDEFINED))
	{
		Point bottomright = (mWidth != ____UNDEFINED) && (mHeight != ____UNDEFINED) ? Point(mPosition.x + mWidth - 1, mPosition.y + mHeight - 1) : IElement::getRightBottom();
		dst = CST_NEW(ViewPort(surface, mPosition, bottomright), "FastPlacement", "destination viewport");
	}
	else
		dst = CST_NEW(MovingPort(surface, mPosition), "FastPlacement", "destination viewport");

	if (Restorable::mStorage)
	{
		Restorable::restore(*dst);
	}
	else
	{
		mMargins = Placement::draw(surface);
		Restorable::backup(*dst);
	}
	CST_DELETE(ISurface, dst);
	return mMargins;
}

void FastPlacement::raise(CST::Common::Event& event)
{
	if (event.getType() == EVENT_TYPE_DRAW_REQUEST)
	{
		Restorable::reset();
	}
	Placement::raise(event);
}

bool FastPlacement::onEvent(CST::Common::Event& event)
{
	if (event.getType() == EVENT_TYPE_SET_DEFAULT)
		reset();
	return Placement::onEvent(event);
}

/////////////////////////////////////////////////////////////////////////////

Margins Substrate::draw(ISurface& surface)
{
	if (!mStorage)
		backup(surface);
	else
		restore(surface);
	return IElement::draw(surface);
}

bool Substrate::onEvent(CST::Common::Event& event)
{
	if (event.getType() == EVENT_TYPE_SET_DEFAULT)
		reset();
	return IElement::onEvent(event);
}
