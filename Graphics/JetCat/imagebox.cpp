/*************************************************************************************/
/* Portable Graphics Library for Embedded Systems * (C) Componentality Oy, 2015-2016 */
/* Initial design and development: Konstantin A. Khait                               */
/* Support, comments and questions: dev@componentality.com                           */
/*************************************************************************************/

#include "imagebox.h"
#include "Surface/bmp_surface.h"
#include "../Drawing/scale.h"

using namespace Componentality::Graphics;
using namespace Componentality::Graphics::JetCat;

Image::Image() : mRead(false)
{
	mImage = NULL;
}

Image::~Image()
{
	if (mImage)
		CST_DELETE(IImageSurface, mImage);
	mImage = NULL;
}

Margins Image::draw(ISurface& surface)
{
	if (!isHidden())
	{
		if (!mRead)
			readToCache();
		if (mRead && mImage)
			surface.apply(*mImage);
	}
	return Margins();
}

void Image::readToCache()
{
	if (!mImage)
		mImage = CST_NEW(BitmapSurface, "Image", "default bitmap surface");
	mRead = mImage->read(mFilename);
}

///////////////////////////////////////////////////////////////////////////////

ImageBox::ImageBox(const size_t width, const size_t height, const bool proportional)
{
	size(width, height);
	mKeepProportions = proportional;
}

ImageBox::ImageBox(const std::string file, const size_t width, const size_t height, const bool proportional)
{
	size(width, height);
	mKeepProportions = proportional;
	mImageFiles.push_back(file);
}

ImageBox::ImageBox(const std::string files[], const size_t width, const size_t height, const bool proportional)
{
	size(width, height);
	mKeepProportions = proportional;
	for (int i = 0; !files[i].empty(); i++)
		mImageFiles.push_back(files[i]);
}

ImageBox::ImageBox(const std::list<std::string> _files, const size_t width, const size_t height, const bool proportional)
{
	size(width, height);
	mKeepProportions = proportional;
	std::list<std::string>& files = (std::list<std::string>&) _files;
	for (std::list<std::string>::iterator i = files.begin(); i != files.end(); i++)
		if (!i->empty())
			mImageFiles.push_back(*i);
}

ImageBox::~ImageBox()
{
}

Margins ImageBox::draw(ISurface& surface)
{
	if (!mRead)
		readToCache();
	if (mRead && mImage)
	{
		double xcoeff = (double)mWidth / (double)mImage->getWidth();
		double ycoeff = (double)mHeight / (double)mImage->getHeight();
		if (mKeepProportions)
			ycoeff = xcoeff = ____min(xcoeff, ycoeff);
		Componentality::Graphics::ScaledSurface scaler(surface, xcoeff, ycoeff);
		Image::draw(scaler);
	}
	return Margins();
}

ImageBox::COMPARISON_RESULT ImageBox::getDifference(BitmapSurface& to_compare)
{
	COMPARISON_RESULT result;
	result.rating = 0;
	result.difference = ____UNDEFINED;
	if ((to_compare.getWidth() == mWidth) && (to_compare.getHeight() == mHeight))
	{
		result.rating = 255;
		result.difference = 0;
	}
	else if ((to_compare.getWidth() >= mWidth) && (to_compare.getHeight() >= mHeight))
	{
		result.rating = 10;
		result.difference = ____max(to_compare.getWidth() - mWidth, to_compare.getHeight() - mHeight);
	}
	else if ((to_compare.getWidth() >= mWidth) && (to_compare.getHeight() < mHeight))
	{
		result.rating = 5;
		result.difference = ____max(to_compare.getWidth() - mWidth, mHeight - to_compare.getHeight());
	}
	else if ((to_compare.getWidth() < mWidth) && (to_compare.getHeight() >= mHeight))
	{
		result.rating = 5;
		result.difference = ____max(mWidth - to_compare.getWidth(), mHeight - to_compare.getHeight());
	}
	else
	{
		result.rating = 1;
		result.difference = ____max(mWidth - to_compare.getWidth(), mHeight - to_compare.getHeight());
	}
	return result;
}
	
void ImageBox::readToCache()
{
	if (mImageFiles.empty())
		return;
	std::string mFile;
	COMPARISON_RESULT maxrating; maxrating.rating = 0; maxrating.difference = ____UNDEFINED;
	for (std::list<std::string>::iterator i = mImageFiles.begin(); i != mImageFiles.end(); i++)
	{
		BitmapSurface bs;
		if (bs.read(*i))
		{
			COMPARISON_RESULT rating = getDifference(bs);
			if ((rating.rating > maxrating.rating) ||
				((rating.rating >= maxrating.rating) && (rating.difference < maxrating.difference)))
			{
				mFile = *i;
				maxrating = rating;
			}
		}
	}
	if (!mFile.empty())
		setFilename(mFile);
	Image::readToCache();
}

void ImageBox::size(const size_t width, const size_t height)
{
	ISizeable::size(width, height);
	readToCache();
}
