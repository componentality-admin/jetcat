/*************************************************************************************/
/* Portable Graphics Library for Embedded Systems * (C) Componentality Oy, 2015-2016 */
/* Initial design and development: Konstantin A. Khait                               */
/* Support, comments and questions: dev@componentality.com                           */
/*************************************************************************************/
/* Main input events for mouse, touchpad, keyboard etc. Primitives for events        */
/* management.                                                                       */
/*************************************************************************************/

#ifndef __INPUTS_H__
#define __INPUTS_H__

#include "drawable.h"
#include "Surface/easy_draw.h"
#include "common/common_events.h"

namespace Componentality
{
	namespace Graphics
	{
		namespace JetCat
		{
			extern const std::string EVENT_TYPE_POINTER;			// Mouse move event
			extern const std::string EVENT_TYPE_PRESS;				// Mouse left button down or touch screen press
			extern const std::string EVENT_TYPE_DEPRESS;			// Mouse left button up or touch screen depress
			extern const std::string EVENT_TYPE_DRAW_REQUEST;		// Request to re-draw the subtree or all the desktop
			extern const std::string EVENT_TYPE_TIMER;				// 100ms timer event
			extern const std::string EVENT_TYPE_MENU;				// Menu item selected
			extern const std::string EVENT_TYPE_KEY;				// Keyboard event
			extern const std::string EVENT_TYPE_SET_DEFAULT;		// Reset graphic caches and other parameters to default

			class Window;

			// Input event abstraction
			class IInputEvent : public CST::Common::Event
			{
			protected:
				bool mNotToModal;										// true if message shall not be sent to non-modal windows if modal is present
				bool mNotToHidden;										// true if message shall not be sent to hidden objects also
			protected:
				IInputEvent(const std::string type) : CST::Common::Event(type), mNotToModal(true), mNotToHidden(true) {};
				IInputEvent() : mNotToModal(true), mNotToHidden(true) {};
			public:
				virtual ~IInputEvent() { };
				// Retrieve type of the event
				virtual std::string getType() const { return mType; }
				// Filter event. If false, the element and it's subordinatories shall not process this event
				// mostly for filtering out touch/mouse events being cause outside of the object
				virtual bool filter(Componentality::Graphics::Point lefttop, Componentality::Graphics::Point rightbottom) = 0;
				// Event shall not be sent to non-modal windows if modal window is displayed
				bool isNotToModal() const { return mNotToModal; }
				// Event shall not be sent to hidden windows
				bool isNotToHidden() const { return mNotToHidden; }
			protected:
				// Set and get payload for the event
				virtual void setPayload(Payload& payload) { if (mPayload) CST_DELETE(Payload, mPayload); mPayload = &payload; }
				virtual Payload* getPayload() const { return mPayload; }
			};

			// Base class for all events from touch devices and mices. Contains coordinates where event is happening
			class PointerEvent : public IInputEvent
			{
			protected:
				PointerEvent(std::string type) : IInputEvent(type) { };
			public:
				class Point : public Componentality::Graphics::Point, public CST::Common::Event::Payload
				{
				public:
					Point(const size_t _x = 0, const size_t _y = 0) : Componentality::Graphics::Point(_x, _y) {};
					virtual ~Point() {};
				};
			public:
				PointerEvent() : IInputEvent(EVENT_TYPE_POINTER) { mPayload = CST_NEW(Point, "PointerEvent", "payload"); };
				PointerEvent(const Componentality::Graphics::Point point) : IInputEvent(EVENT_TYPE_POINTER) { mPayload = CST_NEW(Point, "PointerEvent", "payload"); set(point); };
				PointerEvent(const size_t x, const size_t y) : IInputEvent(EVENT_TYPE_POINTER)
					{ mPayload = CST_NEW(Point, "PointerEvent", "payload"); ((Point*)mPayload)->x = x; ((Point*)mPayload)->y = y; };
				virtual ~PointerEvent() { if (mPayload) CST_DELETE(Point, mPayload); mPayload = NULL; };
				virtual Componentality::Graphics::Point get() const { return *(Point*)mPayload; }
				virtual void set(const Componentality::Graphics::Point point) { ((Point*)mPayload)->x = point.x; ((Point*)mPayload)->y = point.y; }
				virtual bool filter(Componentality::Graphics::Point lefttop, Componentality::Graphics::Point rightbottom);
			};

			// Mouse button press or touch device press event
			class PressEvent : public PointerEvent
			{
			public:
				PressEvent() : PointerEvent(EVENT_TYPE_PRESS) { mPayload = CST_NEW(Point, "PressEvet", "payload"); };
				PressEvent(const Componentality::Graphics::Point point) : PointerEvent(point) { mType = EVENT_TYPE_PRESS; };
				PressEvent(const size_t x, const size_t y) : PointerEvent(x, y) { mType = EVENT_TYPE_PRESS; };
				virtual ~PressEvent() { };
			};

			// Mouse button depress or touch device release event
			class DepressEvent : public PointerEvent
			{
			public:
				DepressEvent() : PointerEvent(EVENT_TYPE_DEPRESS) { mPayload = CST_NEW(Point, "DepressEvent", "payload"); };
				DepressEvent(const Componentality::Graphics::Point point) : PointerEvent(point) { mType = EVENT_TYPE_DEPRESS; };
				DepressEvent(const size_t x, const size_t y) : PointerEvent(x, y) { mType = EVENT_TYPE_DEPRESS; };
				virtual ~DepressEvent() { };
			};

			// One of the major events: request for element or hierarchy redrawing
			class DrawRequestEvent : public IInputEvent
			{
			protected:
				Window* mWindow;				// Window, whose element initiated a request
			public:
				// Initiator is the description of who shall be redrawn. If initiator is set to NULL, all
				// object's hierarchy is to be redrawn
				struct Initiator : public CST::Common::Event::Payload
				{
					Initiator(IElement* initiator) : mInitiator(initiator) {}
					IElement* mInitiator;
				};
			protected:
				DrawRequestEvent(std::string type) : IInputEvent(type) { };
			public:
				DrawRequestEvent(IElement* initiator) : IInputEvent(EVENT_TYPE_DRAW_REQUEST), mWindow(NULL) { setPayload(*CST_NEW(Initiator(initiator), "DrawRequestEvent", "initiator")); };
				DrawRequestEvent() : IInputEvent(EVENT_TYPE_DRAW_REQUEST) { setPayload(*CST_NEW(Initiator(NULL), "DrawRequestEvent", "initiator")); };
				virtual ~DrawRequestEvent() { if (mPayload) CST_DELETE(Initiator, mPayload); mPayload = NULL; };
				virtual Initiator* get() const { if (mPayload) return (Initiator*)mPayload; else return NULL; }
				virtual bool filter(Componentality::Graphics::Point lefttop, Componentality::Graphics::Point rightbottom) { return true; };
				virtual void setWindow(Window* win = NULL) { mWindow = win; }
				virtual Window* getWindow() const { return mWindow; }
			};

			// Regular (normally 100ms) timer event. MUST be filtered out by elements not needing it, otherwise
			// processing of this event takes too much time
			class TimerEvent : public IInputEvent
			{
			public:
				struct Counter : public CST::Common::Event::Payload
				{
					size_t mCounter;
					Counter(const size_t value) { mCounter = 0; }
				};
			protected:
				static Counter mCounter;
			protected:
				TimerEvent(std::string type) : IInputEvent(type) { mNotToHidden = mNotToModal = false; setPayload(*CST_NEW(Counter(0), "TimerEvent", "payload")); };
			public:
				TimerEvent() : IInputEvent(EVENT_TYPE_TIMER) { mNotToHidden = mNotToModal = false; setPayload(*CST_NEW(Counter(0), "TimerEvent", "payload")); };
				virtual ~TimerEvent() { if (mPayload) CST_DELETE(Counter, mPayload); mPayload = NULL; };
				virtual size_t getCounter() const { if (mPayload) return ((Counter*)mPayload)->mCounter; else return 0; }
				virtual bool filter(Componentality::Graphics::Point lefttop, Componentality::Graphics::Point rightbottom) { return true; };
				static size_t getGlobalCounter() { return mCounter.mCounter; }
				static void appendGlobalCounter() { mCounter.mCounter += 1; }
			};

			// Event being sent upon menu item selected
			class MenuEvent : public IInputEvent
			{
			protected:
				struct Payload : public CST::Common::Event::Payload
				{
					IElement* mMenuItem;
					Payload(IElement* item) : mMenuItem(item) {}
				};
			public:
				MenuEvent(IElement* item) : IInputEvent(EVENT_TYPE_MENU) { setPayload(*CST_NEW(Payload(item), "MenuEvent", "payload")); }
				virtual ~MenuEvent() { if (mPayload) CST_DELETE(Payload, mPayload); mPayload = NULL; };
				virtual IElement* getItem() const { if (mPayload) return ((Payload*)mPayload)->mMenuItem; else return NULL; }
				virtual void setItem(IElement* item) const { if (mPayload) ((Payload*)mPayload)->mMenuItem = item; }
				virtual bool filter(Componentality::Graphics::Point lefttop, Componentality::Graphics::Point rightbottom) { return true; };
			};

			// Key press/release event
			class KeyEvent : public IInputEvent
			{
			public:
				enum KEY_TYPE {
					ALPHABETH,
					SPECIAL
				};
				enum EVENT_TYPE
				{
					DOWN,
					UP
				};
			protected:
				struct Payload : public CST::Common::Event::Payload
				{
					int mKeyCode;										// ABC key or special key code
					KEY_TYPE mKeyType;									// Key type
					EVENT_TYPE mEventType;								// Event type
					IElement* mOriginator;								// Message sender
					Payload(IElement* sender, const KEY_TYPE ktype, const EVENT_TYPE etype, const int code) : 
						mOriginator(sender), mKeyCode(code), mKeyType(ktype), mEventType(etype) {}
				};
			public:
				KeyEvent(IElement* item, const KEY_TYPE kt, const EVENT_TYPE et, const int code) : IInputEvent(EVENT_TYPE_KEY) 
					{ setPayload(*CST_NEW(Payload(item, kt, et, code), "KeyEvent", "payload")); }
				virtual ~KeyEvent() { if (mPayload) CST_DELETE(Payload, mPayload); mPayload = NULL; };
				virtual IElement* getOriginator() const { if (mPayload) return ((Payload*)mPayload)->mOriginator; else return NULL; }
				virtual int getKeyCode() const { return ((Payload*)mPayload)->mKeyCode; }
				virtual KEY_TYPE getKeyType() const { return ((Payload*)mPayload)->mKeyType; }
				virtual EVENT_TYPE getEventType() const { return ((Payload*)mPayload)->mEventType; }
				virtual bool filter(Componentality::Graphics::Point lefttop, Componentality::Graphics::Point rightbottom) { return true; };
			public:
				enum VKEYS
				{
					VKEY_LEFT		= -1000,
					VKEY_RIGHT		= -1001,
					VKEY_UP			= -1002,
					VKEY_DOWN		= -1003,
					VKEY_BACKSPACE	= -1004,
					VKEY_DELETE		= -1005,
					VKEY_ENTER		= -1006,
					VKEY_ESCAPE		= -1007,
					VKEY_TAB		= -1008
				};
			};

			// Special event to reset element to it's default state. Normally used by 'owning' elements to inform
			// subordinatories that they should be reset to default
			class SetDefaultEvent : public IInputEvent
			{
			protected:
				SetDefaultEvent(const std::string type) : IInputEvent(type) {};
			public:
				SetDefaultEvent() : IInputEvent(EVENT_TYPE_SET_DEFAULT) { this->mNotToHidden = true; };
				virtual ~SetDefaultEvent() {};
				virtual bool filter(Componentality::Graphics::Point lefttop, Componentality::Graphics::Point rightbottom) { return true; };
			};

		} // namespace JetCat
	} // namespace Graphics
} // namespace Componentality

#endif