/*************************************************************************************/
/* Portable Graphics Library for Embedded Systems * (C) Componentality Oy, 2015-2016 */
/* Initial design and development: Konstantin A. Khait                               */
/* Support, comments and questions: dev@componentality.com                           */
/*************************************************************************************/

#ifndef __EVTFILTER_H__
#define __EVTFILTER_H__

#include "common/common_events.h"
#include <map>
#include <string>

namespace Componentality
{
	namespace Graphics
	{
		namespace JetCat
		{
			class IElement;

			class IEventFilter
			{
			public:
				IEventFilter() {};
				virtual ~IEventFilter() {};
				virtual bool filter(CST::Common::Event&, IElement&) = 0;
			};

			class PointerEventStdFilter : public IEventFilter
			{
			public:
				PointerEventStdFilter() {};
				virtual ~PointerEventStdFilter() {};
				virtual bool filter(CST::Common::Event& event, IElement& element);
			};

			class IEventFilterSet : public IEventFilter
			{
			protected:
				std::map<std::string, IEventFilter*> mFilters;
			public:
				IEventFilterSet() {};
				virtual ~IEventFilterSet() {};
				virtual bool filter(CST::Common::Event&, IElement&);
			public:
				virtual void add(const std::string, IEventFilter*);
				virtual void remove(const std::string);
			};

			class StdEventFilter : public IEventFilterSet
			{
			protected:
				PointerEventStdFilter mPointerFilter;
			public:
				StdEventFilter();
				virtual ~StdEventFilter() {};
			};

			class BlackListEventFilter : public IEventFilter
			{
			protected:
				std::map<std::string, bool> mEventList;
			public:
				BlackListEventFilter();
				BlackListEventFilter(const std::list<std::string>&);
				BlackListEventFilter(const std::string[]);
				virtual ~BlackListEventFilter();
				virtual bool filter(CST::Common::Event&, IElement&);
			};

		} // namespace JetCat
	} // namespace Graphics
} // namespace Componentality

#endif