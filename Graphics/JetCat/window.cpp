/*************************************************************************************/
/* Portable Graphics Library for Embedded Systems * (C) Componentality Oy, 2015-2016 */
/* Initial design and development: Konstantin A. Khait                               */
/* Support, comments and questions: dev@componentality.com                           */
/*************************************************************************************/

#include "window.h"
#include "global.h"

using namespace Componentality::Graphics;
using namespace Componentality::Graphics::JetCat;

Window::Window(WindowManager& owner) 
{
	mPosition.x = mPosition.y = 0;
	mHeight = mWidth = ____UNDEFINED;
	std::string handle;
	void* _this = this;
	handle.assign((char*)&_this, sizeof(this));
	owner.add(handle, *this);
};

Window::~Window() 
{
	std::string handle;
	void* _this = this;
	handle.assign((char*)&_this, sizeof(this));
	mOwner->remove(handle);
};


Margins Window::draw(ISurface& surface)
{
	if (isHidden())
		return Margins();
	if (mWidth == ____UNDEFINED)
		mWidth = surface.getWidth();
	if (mHeight == ____UNDEFINED)
		mHeight = surface.getHeight();
	Point bottomright = (mWidth != ____UNDEFINED) && (mHeight != ____UNDEFINED) ? Point(mPosition.x + mWidth - 1, mPosition.y + mHeight - 1) : IElement::getRightBottom();
	ViewPort vp(surface, mPosition, bottomright);
	mTopLeft.x += mPosition.x; mTopLeft.y += mPosition.y;
	mRightBottom.x = mTopLeft.x + mWidth - 1; mRightBottom.y = mTopLeft.y + mHeight - 1;
	return IElement::draw(vp);
}

void Window::raise(CST::Common::Event& event)
{
	if (event.getType() == EVENT_TYPE_DRAW_REQUEST)												// Draw event has a reduced path
	{
		DrawRequestEvent& dre = (DrawRequestEvent&)event;
		if (!dre.getWindow())
			dre.setWindow(this);
	}
	IElement::raise(event);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

WindowManager::WindowManager() : mSurface(NULL)
{
	subscribe(*this);
	run((CST::Common::thread::threadfunc) __process, this);
	run((CST::Common::thread::threadfunc) __timer, this);
}

WindowManager::WindowManager(ISurface& surface) : mSurface(&surface)
{
	subscribe(*this);
	run((CST::Common::thread::threadfunc) __process, this);
	run((CST::Common::thread::threadfunc) __timer, this);
}

WindowManager::~WindowManager()
{
	unsubscribe(*this);
	exit();
}

void WindowManager::draw()
{
	if (!mSurface)
		return;
	mLock.lock();
	setTopLeft(Point(0, 0));
	setRightBottom(Point(mSurface->getWidth() - 1, mSurface->getHeight() - 1));
	draw(*mSurface);
	mLock.unlock();
}
	
void WindowManager::__process(WindowManager* master)
{
	while (!master->getExit())
	{
		master->mLock.lock();
		if (!master->mQueue.empty())
		{
			master->dispose(master->processEvent());
			master->mLock.unlock();
		}
		else
		{
			master->mLock.unlock();
			CST::Common::sleep(10);
		}
	}
	CST::Common::thread::end();
}

void WindowManager::__timer(WindowManager* master)
{
	while (!master->getExit())
	{
		CST::Common::sleep(100);
		master->sendEvent(*CST_NEW(TimerEvent, "WindowManager", "timer"));
		TimerEvent::appendGlobalCounter();
	}
	CST::Common::thread::end();
}

void WindowManager::setSurface(ISurface& surface)
{
	mLock.lock();
	mSurface = &surface;
	mLock.unlock();
}

ISurface* WindowManager::getSurface() const
{
	mLock.lock();
	ISurface* result = mSurface;
	mLock.unlock();
	return result;
}

void WindowManager::reset()
{
	mLock.lock();
	ISurface* result = NULL;
	mLock.unlock();
}

bool WindowManager::onEvent(CST::Common::Event& event)
{
	if (!filter(event))
		return false;
	if (event.getType() == EVENT_TYPE_KEY)														// Keyboard events are only sent to focus holder
		return sendEventToFocusHolder(event);
	if (event.getType() == EVENT_TYPE_DRAW_REQUEST)												// Draw event has a reduced path
	{
		DrawRequestEvent& dre = (DrawRequestEvent&)event;
		DrawRequestEvent::Initiator* initiator = dre.get();
		if (initiator && initiator->mInitiator && mSurface)
		{
			IElement* to_draw = initiator->mInitiator;
			while (to_draw && to_draw->isMovable())												// Movable objects must be drawn with it's owner
				to_draw = to_draw->getOwner();

			Window* owner_window = dre.getWindow();
			std::pair< ISurface*, std::list<ISurface*> > exclusion = makeExceptionArea(owner_window, *mSurface);
			ViewPort vp(*exclusion.first, to_draw->getTopLeft(), to_draw->getRightBottom());
			to_draw->draw(vp);
			dispose(exclusion.second);
		}
		else 
			draw();
		event.setProcessed();
		return true;
	}
	else
		return IElement::onEvent(event);
}

// This function makes an area where elements of win shall not be drawn.
// It returns surface to draw and list of surfaces to clean up
std::pair< ISurface*, std::list<ISurface*> > WindowManager::makeExceptionArea(Window* win, ISurface& surface)
{
	std::pair< ISurface*, std::list<ISurface*> > result;
	result.first = &surface;

	if (!win)
		return result;

	DrawOrder order = getOrder();
	bool found = false;
	
	mLock.lock();
	for (DrawOrder::iterator i = order.begin(); i != order.end(); i++)
	{
		IDrawable* current = get(*i);
		if (!found)
		{
			if (current == win)
			{
				found = true;
			}
		}
		else
		{
			if (!current->isHidden())
			{
				ExclusionPort* eport = CST_NEW(ExclusionPort(*result.first, current->getTopLeft(), current->getRightBottom()), "WindowManager", "exclusion port");
				result.second.push_back(eport);
				result.first = eport;
			}
		}
	}

	mLock.unlock();
	return result;
}

// Clean the list of surfaces after used
void WindowManager::dispose(std::list<ISurface*>& to_dispose)
{
	for (std::list<ISurface*>::iterator i = to_dispose.begin(); i != to_dispose.end(); i++)
		CST_DELETE(ISurface, *i);
}

//////////////////////////////////////////////////////////////////////////////////////////

bool ModalWindow::onEvent(CST::Common::Event& event)
{
	bool result = Window::onEvent(event);
	if (filter(event))
		if (((IInputEvent&)event).isNotToModal() && !isHidden())				// If message is to modal windows only
			((IInputEvent&)event).setProcessed();								// mark it as processed
	return result;
}

Margins ModalWindow::draw(ISurface& surface)
{
	if (mGreying && !isHidden())
	{
		for (size_t j = 0; j < surface.getHeight(); j++)
			for (size_t i = 0; i < surface.getWidth(); i++)
			{
				Color color = surface.peek(i, j);
				grey(i, j, color);
				surface.plot(i, j, color);
			}
	}
	return Window::draw(surface);
}

void ModalWindow::grey(const size_t x, const size_t y, Color& color)
{
	ColorRGB rgb = color;
	rgb.blue = (char)____min(255, ((int)rgb.blue + 60) * 2);
	rgb.red = (char)____min(255, ((int)rgb.red + 60) * 2);
	rgb.green = (char)____min(255, ((int)rgb.green + 60) * 2);
	color = rgb;
}
