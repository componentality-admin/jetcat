/*************************************************************************************/
/* Portable Graphics Library for Embedded Systems * (C) Componentality Oy, 2015-2016 */
/* Initial design and development: Konstantin A. Khait                               */
/* Support, comments and questions: dev@componentality.com                           */
/*************************************************************************************/

#include "evtfilter.h"
#include "drawable.h"
#include "inputs.h"

using namespace Componentality::Graphics;
using namespace Componentality::Graphics::JetCat;

bool PointerEventStdFilter::filter(CST::Common::Event& event, IElement& element)
{
	PointerEvent* evt = (PointerEvent*)&event;
	return evt->filter(element.getTopLeft(), element.getRightBottom());
}

bool IEventFilterSet::filter(CST::Common::Event& evt, IElement& element)
{
	std::map<std::string, IEventFilter*>::iterator ptr = mFilters.find(evt.getType());
	if (ptr != mFilters.end())
		return ptr->second->filter(evt, element);
	return true;			// Filter must pass all the messages which it doesn't know how to filter
}

void IEventFilterSet::add(const std::string type, IEventFilter* filter)
{
	mFilters[type] = filter;
}

void IEventFilterSet::remove(const std::string type)
{
	if (mFilters.find(type) != mFilters.end())
		mFilters.erase(mFilters.find(type));
}

StdEventFilter::StdEventFilter()
{
	add(EVENT_TYPE_POINTER, &mPointerFilter);
	add(EVENT_TYPE_PRESS, &mPointerFilter);
	add(EVENT_TYPE_DEPRESS, &mPointerFilter);
}

/////////////////////////////////////////////////////////////////////////////////

BlackListEventFilter::BlackListEventFilter()
{
}

BlackListEventFilter::BlackListEventFilter(const std::list<std::string>& _flt)
{
	std::list<std::string>& flt = (std::list<std::string>&) _flt;

	for (std::list<std::string>::iterator i = flt.begin(); i != flt.end(); i++)
		mEventList[*i] = true;
}

BlackListEventFilter::BlackListEventFilter(const std::string flt[])
{
	for (size_t i = 0; !flt[i].empty(); i++)
		mEventList[flt[i]] = true;
}

BlackListEventFilter::~BlackListEventFilter()
{
}

bool BlackListEventFilter::filter(CST::Common::Event& event, IElement&)
{
	return mEventList.find(event.getType()) == mEventList.end();
}
