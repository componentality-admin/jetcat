/********************************************************************************/
/* Portable Graphics Library for Embedded Systems * (C) Componentality Oy, 2015 */
/* Initial design and development: Konstantin A. Khait                          */
/* Support, comments and questions: dev@componentality.com                      */
/********************************************************************************/

#include "surface.h"

using namespace Componentality::Graphics;

Color::Color()
{
	mType = COLOR_UNDEFINED;
	mAlpha = 0;
}

Color::Color(const ColorRGB ColorRGB, const unsigned char alpha)
{
	operator=(ColorRGB);
	mAlpha = alpha;
}

Color::Color(const unsigned char red, const unsigned char green, const unsigned char blue, const unsigned char alpha)
{
	ColorRGB ColorRGB;
	ColorRGB.blue = blue; ColorRGB.green = green; ColorRGB.red = red;
	operator=(ColorRGB);
	mAlpha = alpha;
}

Color::Color(const Greyscale intensity, const unsigned char alpha)
{
	operator=(intensity);
	mAlpha = alpha;
}

Color::Color(const unsigned char greyscale, const unsigned char alpha)
{
	Greyscale gs;
	gs.intensity = greyscale;
	operator=(gs);
	mAlpha = alpha;
}

Color::Color(const Color& source)
{
	mType = source.mType;
	if (mType == COLOR_ColorRGB)
	{
		mColorRGB = source.mColorRGB;
		mAlpha = source.mAlpha;
	}
}

Color::~Color()
{
}

bool Color::operator==(const Color& src) const
{
	return (mColorRGB.red == src.mColorRGB.red) &&
		(mColorRGB.green == src.mColorRGB.green) &&
		(mColorRGB.blue == src.mColorRGB.blue) &&
		(mAlpha == src.mAlpha);
}

bool Color::operator!=(const Color& src) const
{
	return !operator==(src);
}

Color& Color::operator=(const ColorRGB ColorRGB)
{
	mType = COLOR_ColorRGB;
	mColorRGB = ColorRGB;
	mAlpha = 255;
	return *this;
}

Color& Color::operator=(const Greyscale intensity)
{
	mColorRGB.red = mColorRGB.green = mColorRGB.blue = intensity.intensity;
	mAlpha = 255;
	return *this;
}

Color& Color::operator=(const Color& source)
{
	mType = source.mType;
	if (mType == COLOR_ColorRGB)
	{
		mColorRGB = source.mColorRGB;
		mAlpha = source.mAlpha;
	}
	return *this;;
}

Color::operator Greyscale() const
{
	if (mType == COLOR_ColorRGB)
	{
		// Recalculation of ColorRGB color to greyscale according to color's visibility coefficients
		double value = 0.2126 * (double)mColorRGB.red + 0.7152 * (double)mColorRGB.green + 0.0722 * (double)mColorRGB.blue;
		Greyscale result;
		result.intensity = (unsigned char)(int)value;
		return result;
 	}
	Greyscale result;
	result.intensity = 0;
	return result;
}

Color::operator ColorRGB() const
{
	if (mType == COLOR_ColorRGB)
		return mColorRGB;
	ColorRGB result;
	result.red = result.green = result.blue = 0;
	return result;
}

void ISurface::apply(ISurface& source)
{
	onApply();
	source.onApply();
	size_t source_width = source.getWidth();
	size_t source_height = source.getHeight();
	size_t width = getWidth();
	size_t height = getHeight();
	for (size_t i = 0; i < ____min(source_width, width); i++)
		for (size_t j = 0; j < ____min(source_height, height); j++)
			plot(i, j, source.peek(i, j));
	source.onApplied();
	onApplied();
}

const Color Componentality::Graphics::COLOR_RED(255, 0, 0);
const Color Componentality::Graphics::COLOR_GREEN(0, 128, 0);
const Color Componentality::Graphics::COLOR_BLUE(0, 0, 255);
const Color Componentality::Graphics::COLOR_WHITE(255, 255, 255);
const Color Componentality::Graphics::COLOR_BLACK(0, 0, 0);
const Color Componentality::Graphics::COLOR_YELLOW(255, 255, 0);
const Color Componentality::Graphics::COLOR_BROWN(165, 42, 42);
const Color Componentality::Graphics::COLOR_GREY(128, 128, 128);
const Color Componentality::Graphics::COLOR_SILVER(192, 192, 192);
const Color Componentality::Graphics::COLOR_MAROON(128, 0, 0);
const Color Componentality::Graphics::COLOR_OLIVE(128, 128, 0);
const Color Componentality::Graphics::COLOR_LIME(0, 255, 0);
const Color Componentality::Graphics::COLOR_AQUA(0, 255, 255);
const Color Componentality::Graphics::COLOR_NAVY(0, 0, 128);
const Color Componentality::Graphics::COLOR_FICHSIA(255, 0, 255);
const Color Componentality::Graphics::COLOR_PURPLE(128, 0, 128);
const Color Componentality::Graphics::COLOR_TEAL(0, 128, 128);


