// TestCodeWindows.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "TestCodeWindows.h"
#include "../Surface/bmp_surface.h"
#include "../Drawing/brush.h"
#include "../Drawing/viewport.h"
#include "../JetCat/shadow.h"
#include "../JetCat/frame.h"
#include "../JetCat/background.h"
#include "../JetCat/caption.h"
#include "../JetCat/statusbar.h"
#include "../JetCat/progress.h"
#include "../JetCat/imagebox.h"
#include "../JetCat/inputs.h"
#include "../JetCat/window.h"
#include "../JetCat/evtfilter.h"
#include "../JetCat/placement.h"
#include "../JetCat/buttons.h"
#include "../JetCat/menu.h"
#include "../JetCat/keypad.h"
#include "../JetCat/editbox.h"
#include "../JetCat/messagebox.h"
#include "../framebuf/bitmapfb.h"
#include "../framebuf/paw.h"
#include "../JetCat-Modules/touch.h"
#include "../JetCat-Modules/touch_scroll.h"
#include "../JetCat-Modules/layout.h"
#include "../JetCat-Modules/png_surface.h"
#include "../JetCat-Modules/rotator.h"
#include "common/common_memory.h"
#include "Integration.h"
#include <list>

using namespace Componentality::Graphics;
using namespace Componentality::Graphics::JetCat;

class MessageShow : public ActiveArea
{
protected:
	std::list<::Point> mPoints;
	StdEventFilter filter;
public:
	MessageShow(IElement* owner) { setEventFilter(&filter); };
	virtual ~MessageShow() {};
	Margins draw(::ISurface& surface)
	{
		Drawer drawer(surface);
		for (std::list<::Point>::iterator i = mPoints.begin(); i != mPoints.end(); i++)
		{
			Point point = *i;
			Point shift = getTopLeft();
			point = point - shift;
			drawer.point(point, Color(0xFF, 0xFF, 0xFF));
		}
		return IElement::draw(surface);
	}
	bool onEvent(CST::Common::Event& event)
	{
/*		if (event.getType() == EVENT_TYPE_POINTER)
		{
			::PointerEvent& _event = (::PointerEvent&) event;
			mPoints.push_back(_event.get());
			raise(*CST_NEW(DrawRequestEvent(this), "global", "pointer event (track)"));
		}
*/		return IElement::onEvent(event);
	}
	protected: 
		void onTimer() {}
};

IElement& makeCursorButtons()
{
	std::string path = "C:\\Componentality\\mhui\\Resources\\Icons";
	Placement& placement_left = *CST_NEW(Placement, "makeCursorButtons", "placement_left");
	Placement& placement_right = *CST_NEW(Placement, "makeCursorButtons", "placement_right");
	Placement& placement_up = *CST_NEW(Placement, "makeCursorButtons", "placement_up");
	Placement& placement_down = *CST_NEW(Placement, "makeCursorButtons", "placement_down");

	KeyButton& left = *CST_NEW(KeyButton(
		CST::Common::fileJoinPaths(path, "left.bmp"),
		CST::Common::fileJoinPaths(path, "left_pressed.bmp"),
		CST::Common::fileJoinPaths(path, "left.bmp"),
		Color(0, 0, 0),
		Color(0, 0, 0),
		Color(0, 0, 0),
		KeyEvent::SPECIAL,
		KeyEvent::VKEY_LEFT,
		""
		), "global", "KeyButton");

	KeyButton& right = *CST_NEW(KeyButton(
		CST::Common::fileJoinPaths(path, "right.bmp"),
		CST::Common::fileJoinPaths(path, "right_pressed.bmp"),
		CST::Common::fileJoinPaths(path, "right.bmp"),
		Color(0, 0, 0),
		Color(0, 0, 0),
		Color(0, 0, 0),
		KeyEvent::SPECIAL,
		KeyEvent::VKEY_RIGHT,
		""
		), "global", "KeyButton");

	KeyButton& up = *CST_NEW(KeyButton(
		CST::Common::fileJoinPaths(path, "up.bmp"),
		CST::Common::fileJoinPaths(path, "up_pressed.bmp"),
		CST::Common::fileJoinPaths(path, "up.bmp"),
		Color(0, 0, 0),
		Color(0, 0, 0),
		Color(0, 0, 0),
		KeyEvent::SPECIAL,
		KeyEvent::VKEY_UP,
		""
		), "global", "KeyButton");

	KeyButton& down = *CST_NEW(KeyButton(
		CST::Common::fileJoinPaths(path, "down.bmp"),
		CST::Common::fileJoinPaths(path, "down_pressed.bmp"),
		CST::Common::fileJoinPaths(path, "down.bmp"),
		Color(0, 0, 0),
		Color(0, 0, 0),
		Color(0, 0, 0),
		KeyEvent::SPECIAL,
		KeyEvent::VKEY_DOWN,
		""
		), "global", "KeyButton");

	placement_left.add("button", left);
	placement_right.add("button", right);
	placement_up.add("button", up);
	placement_down.add("button", down);

	Placement& placement = *CST_NEW(Placement, "global", "placement");
	placement.add("left", placement_left);
	placement.add("right", placement_right);
	placement.add("up", placement_up);
	placement.add("down", placement_down);

	placement_left.move(0, 28);
	placement_right.move(140, 28);
	placement_up.move(70, 0);
	placement_down.move(70, 70);

	return placement;
}

Placement& makeLayoutTest()
{
	Placement& rotoplace = *CST_NEW(Placement, "global", "roto place");
	Rotator& roto = *CST_NEW(Rotator(10.0, Point(600, 300)), "global", "rotator");
	GridLayout& vl1 = *CST_NEW(GridLayout(3, 2, ____UNDEFINED, ____UNDEFINED, false), "global", "inner layout");
	Placement& pl11 = *CST_NEW(Placement, "global", "placement");
	Placement& pl12 = *CST_NEW(Placement, "global", "placement");
	Placement& pl13 = *CST_NEW(Placement, "global", "placement");
	Placement& pl21 = *CST_NEW(Placement, "global", "placement");
	Placement& pl22 = *CST_NEW(Placement, "global", "placement");
	Placement& pl23 = *CST_NEW(Placement, "global", "placement");
	Background& bgr1 = *CST_NEW(Background(COLOR_RED), "global", "bg1");
	Background& bgg1 = *CST_NEW(Background(COLOR_GREEN), "global", "bg2");
	Background& bgb1 = *CST_NEW(Background(COLOR_BLUE), "global", "bg3");
	Background& bgr2 = *CST_NEW(Background(COLOR_YELLOW), "global", "bg1");
	Background& bgg2 = *CST_NEW(Background(COLOR_BROWN), "global", "bg2");
	Background& bgb2 = *CST_NEW(Background(COLOR_GREY), "global", "bg3");
	pl11.add("red", bgr1);
	pl12.add("green", bgg1);
	pl13.add("blue", bgb1);
	pl21.add("red", bgr2);
	pl22.add("green", bgg2);
	pl23.add("blue", bgb2);
	pl11.size(10, 10);
	pl12.size(10, 15);
	pl13.size(15, 10);
	pl21.size(20, 10);
	pl22.size(10, 20);
	pl23.size(20, 20);
	vl1.add(0, 0, pl11);
	vl1.add(1, 0, pl12);
	vl1.add(2, 0, pl13);
	vl1.add(0, 1, pl21);
	vl1.add(1, 1, pl22);
	vl1.add(2, 1, pl23);
	vl1.size(240, 280);
	vl1.move(600, 300);
	rotoplace.add("roto", roto);
	roto.add("roto", vl1);
	return rotoplace;
}

#define MAX_LOADSTRING 100


// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_TESTCODEWINDOWS, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_TESTCODEWINDOWS));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TESTCODEWINDOWS));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_TESTCODEWINDOWS);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_CREATE:
	{
		Windows::init(hWnd);

		Shadow& shadow = *CST_NEW(Shadow, "root", "shadow");
		Frame& frame = *CST_NEW(Frame, "root", "frame");
		Background& background = *CST_NEW(Background, "root", "background");
		Caption& caption = *CST_NEW(Caption, "root", "caption");
		StatusBar& status = *CST_NEW(StatusBar, "root", "status");
		caption.setTitle("Test Window: it is a very-very long header to be cut off");
		status.setTitle("Status line");
		Window& painter = *CST_NEW(Window(*::Windows::DEFAULT_WINDOW_MANAGER), "root", "window");
		TouchCalibrator& tcal = *CST_NEW(TouchCalibrator, "root", "touch calibrator");
		Container& tep = *CST_NEW(Container, "root", "event processor");
		HTTPCapturer& httpc = *CST_NEW(HTTPCapturer((Componentality::Graphics::Windows::WindowManager&)*::Windows::DEFAULT_WINDOW_MANAGER), "root", "http capturer");
		httpc.add("touch", tcal);
		painter.add("httpc", httpc);
		tcal.add("touch", tep);

		tep.add("shadow", shadow);
		tep.add("frame", frame);
		tep.add("caption", caption);
		tep.add("status", status);
		tep.add("background", background);

		((Background*)tep.get("background"))->setColor(Color(50, 50, 50));
		((Shadow*)tep.get("shadow"))->setWidth(3);
		((Frame*)tep.get("frame"))->setWidth(1);

		painter.move(0, 0);

		Placement& placement1 = *CST_NEW(Placement, "root", "placement");
		tep.add("placement1", placement1);
		placement1.move(200, 200);
		placement1.size(200, 200);

		Image& image = *CST_NEW(Image("c:\\temp\\test.bmp"), "root", "image");
		placement1.add("image0", image);

		Placement& multiline_placement = *CST_NEW(Placement(50, 50, 100, 100), "root", "multiline placement");
		MultilineLabel& mlabel = *CST_NEW(MultilineLabel("Multiline\nLabel\n#1"), "root", "multiline label");
		mlabel.setColor(::Color(255, 255, 255));
		mlabel.setAlignment(IAlignmentDefinitions::Alignments(ITextOutput::H_CENTER, ITextOutput::V_TOP));
		multiline_placement.add("mll", mlabel);
		placement1.add("mlp", multiline_placement);

		Placement& placement2 = *CST_NEW(Scroller(10, 10), "root", "scroller");
		background.add("placement2", placement2);
		placement2.move(50, 100);
		placement2.size(100, 30);

		ImageBox& pig = *CST_NEW(ImageBox, "root", "pig");
		PNGImageSurface& _pis = *CST_NEW(PNGImageSurface, "root", "pis");
		_pis.read("c:\\temp\\3b-char.png");
		pig.setImage(_pis, true);
		placement2.add("pis", pig);

		Placement& placement3 = *CST_NEW(Placement, "root", "placement");
		background.add("placement3", placement3);
		placement3.move(50, 200);
		placement3.size(30, 100);

		ProgressIndicator& progress = *CST_NEW(ProgressIndicator(
			ProgressIndicator::HORIZONTAL,
			Color(255, 255, 255),
			Color(80, 80, 80),
			Margins(10, 5, 10, 5),
			Color(0, 0, 0),
			Color(80, 80, 80)), "root", "progress");
		progress.indicate(50);
		ProgressIndicator& progressv = *CST_NEW(ProgressIndicator(
			ProgressIndicator::VERTICAL,
			Color(255, 255, 255),
			Color(80, 80, 80),
			Margins(5, 5, 5, 5),
			Color(0, 0, 0),
			Color(255, 255, 255)), "root", "progress");
		progressv.indicate(35);

//		placement2.add("progress", progress);
//		placement3.add("progressv", progressv);

		{
			TextButton& tbutton = *CST_NEW(TextButton(
				"OK",
				Color(255, 255, 255),
				Color(0, 255, 0),
				Color(0, 0, 0)), "root", "text button");
			Placement& tb_placement = *CST_NEW(Placement, "root", "text button placement");
			tb_placement.add("button", tbutton);
			tb_placement.move(250, 250);
			tb_placement.size(80, 30);

			TextButton& cbutton = *CST_NEW(TextButton(
				"Cancel",
				Color(255, 255, 255),
				Color(255, 0, 0),
				Color(0, 0, 0)), "root", "cbutton");
			MessageWindow* mbox = CST_NEW(MessageWindow(
				*Windows::DEFAULT_WINDOW_MANAGER, true), "root", "message window");
			mbox->setText("New device found");
			mbox->addButton("OK", tbutton);
			mbox->addButton("Cancel", cbutton);
			mbox->hide();
		}

		if (1)
		{
			Placement& menuholder = *CST_NEW(Placement, "root", "menuholder");
			Container& menucontainer = *CST_NEW(Container, "root", "menuholder");
			VerticalMenu &menu = *CST_NEW(VerticalTouchMenu(*::Windows::DEFAULT_WINDOW_MANAGER, 150), "root", "vmenu");
			TextMenuItem& item1_unsel = *CST_NEW(TextMenuItem(menu,
				Color(255, 255, 255), Color(0, 0, 0), "Item 1"), "root", "vmenu item1");
			TextMenuItem& item1_sel = *CST_NEW(TextMenuItem(menu,
				Color(0, 0, 0), Color(255, 255, 255), "Item 1"), "root", "vmenu item1");
			TextMenuItem& item2_unsel = *CST_NEW(TextMenuItem(menu,
				Color(255, 255, 255), Color(0, 0, 0), "Item 2"), "root", "vmenu item2");
			TextMenuItem& item2_sel = *CST_NEW(TextMenuItem(menu,
				Color(0, 0, 0), Color(255, 255, 255), "Item 2"), "root", "vmenu item2");
			TextMenuItem& item3_unsel = *CST_NEW(TextMenuItem(menu,
				Color(255, 255, 255), Color(0, 0, 0), "Item 3"), "root", "vmenu item3");
			TextMenuItem& item3_sel = *CST_NEW(TextMenuItem(menu,
				Color(0, 0, 0), Color(255, 255, 255), "Item 3"), "root", "vmenu item3");
			TextMenuItem& item4_unsel = *CST_NEW(TextMenuItem(menu,
				Color(255, 255, 255), Color(0, 0, 0), "Item 4"), "root", "vmenu item4");
			TextMenuItem& item4_sel = *CST_NEW(TextMenuItem(menu,
				Color(0, 0, 0), Color(255, 255, 255), "Item 4"), "root", "vmenu item4");
			TextMenuItem& item5_unsel = *CST_NEW(TextMenuItem(menu,
				Color(255, 255, 255), Color(0, 0, 0), "Item 5"), "root", "vmenu item4");
			TextMenuItem& item5_sel = *CST_NEW(TextMenuItem(menu,
				Color(0, 0, 0), Color(255, 255, 255), "Item 5"), "root", "vmenu item4");
			menuholder.add("menu", menucontainer);
			menucontainer.add("menu", menu);
			menu.add("item 1", item1_unsel, item1_sel, 40);
			menu.add("item 2", item2_unsel, item2_sel, 40);
			menu.add("item 3", item3_unsel, item3_sel, 40);
			menu.add("item 4", item4_unsel, item4_sel, 40);
			menu.add("item 5", item5_unsel, item5_sel, 40);
			menu.select("item 3");
			menuholder.move(150, 150);
			menuholder.size(100, 140);

			Placement& hmenuholder = *CST_NEW(Placement, "root", "h menu holder");
			IHorizontalMenu &hmenu = *CST_NEW(HorizontalTouchMenu(*::Windows::DEFAULT_WINDOW_MANAGER, 30), "root", "hmenu");
			TextMenuItem& hitem1_unsel = *CST_NEW(TextMenuItem(hmenu,
				Color(255, 255, 255), Color(0, 0, 0), "Item 1"), "root", "hmenu item1");
			TextMenuItem& hitem1_sel = *CST_NEW(TextMenuItem(hmenu,
				Color(0, 0, 0), Color(255, 255, 255), "Item 1"), "root", "hmenu item1");
			TextMenuItem& hitem2_unsel = *CST_NEW(TextMenuItem(hmenu,
				Color(255, 255, 255), Color(0, 0, 0), "Item 2"), "root", "hmenu item2");
			TextMenuItem& hitem2_sel = *CST_NEW(TextMenuItem(hmenu,
				Color(0, 0, 0), Color(255, 255, 255), "Item 2"), "root", "hmenu item2");
			TextMenuItem& hitem3_unsel = *CST_NEW(TextMenuItem(hmenu,
				Color(255, 255, 255), Color(0, 0, 0), "Item 3"), "root", "hmenu item3");
			TextMenuItem& hitem3_sel = *CST_NEW(TextMenuItem(hmenu,
				Color(0, 0, 0), Color(255, 255, 255), "Item 3"), "root", "hmenu item3");
			hmenuholder.add("hmenu", hmenu);
			hmenu.add("item 1", hitem1_unsel, hitem1_sel, 140);
			hmenu.add("item 2", hitem2_unsel, hitem2_sel, 240);
			hmenu.add("item 3", hitem3_unsel, hitem3_sel, 340);
			hmenu.select("item 2");
			hmenuholder.move(50, 50);
			hmenuholder.size(600, 30);

			//menu.setSelectionStrategy(::IMenu::MULTIPLE);

			tep.add("menuholder", menuholder);
			tep.add("hmenuholder", hmenuholder);

			Placement& placement_l = *CST_NEW(Placement, "root", "placement");
			placement_l.move(500, 300);
			placement_l.size(80, 28);
			tep.add("placement_l", placement_l);
			EditBox& eb = *CST_NEW(EditBox(*Windows::DEFAULT_WINDOW_MANAGER, "Default Text",
				Color(0, 0, 0)), "root", "edit box");
			eb.setCursorColor(Color(0, 0, 0));
			eb.setFrameColor(Color(0, 0, 0));
			eb.setBackgroundColor(Color(128, 128, 128));
			eb.setPosition(3);
			placement_l.add("editbox", eb);
		}

		if (0)
		{
			std::string path = "C:\\Componentality\\mhui\\Resources\\Icons";
			StandardKeypad* ak = CST_NEW(StandardKeypad
				(40, 35, 20,
					CST::Common::fileJoinPaths(path, "button.bmp"),
					CST::Common::fileJoinPaths(path, "button.bmp"),
					CST::Common::fileJoinPaths(path, "button.bmp"),
					CST::Common::fileJoinPaths(path, "space_button.bmp"),
					CST::Common::fileJoinPaths(path, "space_button.bmp"),
					CST::Common::fileJoinPaths(path, "space_button.bmp"),
					CST::Common::fileJoinPaths(path, "special_button.bmp"),
					CST::Common::fileJoinPaths(path, "special_button.bmp"),
					CST::Common::fileJoinPaths(path, "special_button.bmp"),
					Color(255, 255, 255),
					Color(80, 80, 80),
					Color(80, 80, 80)), "root", "standard keypad");
			Placement* keyplace = CST_NEW(Placement, "root", "keyplace");
			tep.add("keypad", *keyplace);
			keyplace->add("key1", *ak);

			tep.add("cursor", makeCursorButtons());
		}

		tep.add("layouts", makeLayoutTest());

		ImageBox* ib = CST_NEW(ImageBox(
			{ 
			std::string("c:\\componentality\\mhui\\resources\\icons\\cctv_50x50.bmp"),
			std::string("c:\\componentality\\mhui\\resources\\icons\\cctv_80x80.bmp"),
			std::string()}, 
			30, 30), "root", "imagebox");
		Placement* pl = CST_NEW(Placement, "root", "image placement");
		pl->add("image", *ib);
		pl->move(500, 50);
		tep.add("image", *pl);
		PNGImageSurface& pis = *CST_NEW(PNGImageSurface, "root", "PNG");
		pis.read("c:\\temp\\11.png");
		pis.write("c:\\temp\\12.png");
		BitmapSurface bmps(pis.getWidth(), pis.getHeight());
		bmps.apply(pis);
		bmps.write("c:\\temp\\11.bmp");
		ib->setImage(pis, true);

		Windows::DEFAULT_WINDOW_MANAGER->draw();

		painter.setEventFilter(CST_NEW(StdEventFilter, "root", "event filter"));

		break;
	}
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		
		Windows::draw(hdc);

		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_MOUSEMOVE:
	{
		int xPos = GET_X_LPARAM(lParam);
		int yPos = GET_Y_LPARAM(lParam);
		PointerEvent* event = CST_NEW(PointerEvent(
			Point(xPos, yPos)), "root", "PointerEvent");
		Windows::DEFAULT_WINDOW_MANAGER->sendEvent(*event);
		break;
	}
	case WM_LBUTTONDOWN:
	{
		int xPos = GET_X_LPARAM(lParam);
		int yPos = GET_Y_LPARAM(lParam);
		::PointerEvent* event = CST_NEW(PressEvent(
			::Point(xPos, yPos)), "root", "PressEvent");
		::Windows::DEFAULT_WINDOW_MANAGER->sendEvent(*event);
		break;
	}
	case WM_LBUTTONUP:
	{
		int xPos = GET_X_LPARAM(lParam);
		int yPos = GET_Y_LPARAM(lParam);
		PointerEvent* event = CST_NEW(DepressEvent(
			Point(xPos, yPos)), "root", "DepressEvent");
		Windows::DEFAULT_WINDOW_MANAGER->sendEvent(*event);
		break;
	}
	case WM_KEYUP:
		if (wParam == VK_F1)
		{
			CST::Common::Memory::getInstance().report("C:\\temp\\memdump.txt");
			CST::Common::Memory::getInstance().report("C:\\temp\\memdump.csv", true);
		}
		if (wParam == VK_F2)
		{
			CST::Common::Memory::getInstance().start_diagnostic();
		}
		if (wParam == VK_F3)
		{
			CST::Common::Memory::getInstance().stop_diagnostic();
			CST::Common::Memory::getInstance().report_diagnostic("C:\\temp\\memdiag.txt");
			CST::Common::Memory::getInstance().report_diagnostic("C:\\temp\\memdiag.csv", true);
		}
	case WM_KEYDOWN:
	{
		int keycode = 0;
		if (wParam == VK_UP)
			keycode = KeyEvent::VKEY_UP;
		if (wParam == VK_DOWN)
			keycode = KeyEvent::VKEY_DOWN;
		if (wParam == VK_LEFT)
			keycode = KeyEvent::VKEY_LEFT;
		if (wParam == VK_RIGHT)
			keycode = KeyEvent::VKEY_RIGHT;
		if (wParam == VK_BACK)
			keycode = KeyEvent::VKEY_BACKSPACE;
		if (wParam == VK_DELETE)
			keycode = KeyEvent::VKEY_DELETE;
		if (wParam == VK_ESCAPE)
			keycode = KeyEvent::VKEY_ESCAPE;
		if (wParam == VK_RETURN)
			keycode = KeyEvent::VKEY_ENTER;
		if (wParam == VK_TAB)
			keycode = KeyEvent::VKEY_TAB;
		if (keycode)
		{
			KeyEvent* event = CST_NEW(KeyEvent
				(NULL, KeyEvent::SPECIAL,
					message == WM_KEYUP ? KeyEvent::UP : KeyEvent::DOWN,
					keycode), "root", "KeyEvent");
			Windows::DEFAULT_WINDOW_MANAGER->sendEvent(*event);
		}

		break;
	}
	case WM_CHAR:
	{
		{
			KeyEvent* event = CST_NEW(KeyEvent
				(NULL, KeyEvent::ALPHABETH,
					message == WM_KEYUP ? KeyEvent::UP : KeyEvent::DOWN,
					wParam), "root", "KeyEvent");
			Windows::DEFAULT_WINDOW_MANAGER->sendEvent(*event);
		}
	}
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
