/********************************************************************************/
/* Portable Graphics Library for Embedded Systems * (C) Componentality Oy, 2015 */
/* Initial design and development: Konstantin A. Khait                          */
/* Support, comments and questions: dev@componentality.com                      */
/********************************************************************************/
/* Fragmented surface output (view ports, moving ports, limitators              */
/********************************************************************************/
#include "viewport.h"
#include <map>
#include <math.h>

using namespace Componentality::Graphics;

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

ViewPort::ViewPort(ISurface& master, const Point topleft, const Point bottomright) : mSurface(master)
{
	mXOffset = topleft.x;
	mYOffset = topleft.y;
	mWidth = bottomright.x - topleft.x + 1;
	mHeight = bottomright.y - topleft.y + 1;
}

ViewPort::~ViewPort()
{
}

void ViewPort::plot(const size_t x, const size_t y, const Color& color)
{
	if ((x >= mWidth) || (y >= mHeight))
		onErrorBoundaries(x, y);
	else
		mSurface.plot(x + mXOffset, y + mYOffset, color);
}

Color ViewPort::peek(const size_t x, const size_t y)
{
	if ((x >= mWidth) || (y >= mHeight))
	{
		onErrorBoundaries(x, y);
		return Color();
	}
	else
		return mSurface.peek(x + mXOffset, y + mYOffset);
}

////////////////////////////////////////////////////////////////////

LimitingPort::LimitingPort(ISurface& master, const Point topleft, const Point bottomright) : mSurface(master)
{
	mXOffset = topleft.x;
	mYOffset = topleft.y;
	mWidth = bottomright.x - topleft.x;
	mHeight = bottomright.y - topleft.y;
}

LimitingPort::~LimitingPort()
{
}

void LimitingPort::plot(const size_t x, const size_t y, const Color& color)
{
	if ((x >= mXOffset + mWidth) || (y >= mYOffset + mHeight) || (x < mXOffset) || (y < mYOffset))
		onErrorBoundaries(x, y);
	else
		mSurface.plot(x, y, color);
}

Color LimitingPort::peek(const size_t x, const size_t y)
{
	if ((x >= mXOffset + mWidth) || (y >= mYOffset + mHeight) || (x < mXOffset) || (y < mYOffset))
	{
		onErrorBoundaries(x, y);
		return Color();
	}
	else
		return mSurface.peek(x, y);
}

////////////////////////////////////////////////////////////////////

MovingPort::MovingPort(ISurface& master, const int xshift, const int yshift) : mSurface(master)
{
	mXOffset = xshift;
	mYOffset = yshift;
}

MovingPort::~MovingPort()
{
}

void MovingPort::plot(const size_t x, const size_t y, const Color& color)
{
	int xc = (int)x + mXOffset;
	int yc = (int)y + mYOffset;
	if ((xc >= 0) && (yc >= 0))
		mSurface.plot(xc, yc, color);
}

Color MovingPort::peek(const size_t x, const size_t y)
{
	int xc = (int)x + mXOffset;
	int yc = (int)y + mYOffset;
	return mSurface.peek(xc, yc);
}

////////////////////////////////////////////////////////////////////////

void MeasuringSurface::plot(const size_t x, const size_t y, const Color&)
{
	if (x < mXMin)
		mXMin = x;
	if (y < mYMin)
		mYMin = y;
	if (x > mXMax)
		mXMax = x;
	if (y > mYMax)
		mYMax = y;
}

Color MeasuringSurface::peek(const size_t x, const size_t y)
{
	if (x < mXMin)
		mXMin = x;
	if (y < mYMin)
		mYMin = y;
	if (x > mXMax)
		mXMax = x;
	if (y > mYMax)
		mYMax = y;
	return NullSurface::peek(x, y);
}

void MeasuringSurface::reset()
{
	mXMax = mYMax = 0;
	mXMin = mYMin = ____UNDEFINED;
}

////////////////////////////////////////////////////////////////////

ExclusionPort::ExclusionPort(ISurface& master, const Point topleft, const Point bottomright) : LimitingPort(master, topleft, bottomright)
{
}

ExclusionPort::~ExclusionPort()
{
}

void ExclusionPort::plot(const size_t x, const size_t y, const Color& color)
{
	if ((x >= mXOffset + mWidth) || (y >= mYOffset + mHeight) || (x < mXOffset) || (y < mYOffset))
		mSurface.plot(x, y, color);
	else
		onErrorBoundaries(x, y);
}

Color ExclusionPort::peek(const size_t x, const size_t y)
{
	if ((x >= mXOffset + mWidth) || (y >= mYOffset + mHeight) || (x < mXOffset) || (y < mYOffset))
		return mSurface.peek(x, y);
	else
	{
		onErrorBoundaries(x, y);
		return Color();
	}
}

//////////////////////////////////////////////////////////////////////////////////////

void RotationPort::plot(const size_t _x, const size_t _y, const Color& color)
{
	Point a0 = rotate(_x, _y);
	mMaster.plot(a0.x, a0.y, color);
	for (int i = -2; i < 2; i++)
	{
		Point d = original(a0.x + i, a0.y);
		if ((d.x == _x) && (d.y == _y) && !color.isUndefined())
			mMaster.plot(a0.x + i, a0.y, color);
	}
	for (int j = -2; j < 2; j++)
	{
		Point d = original(a0.x, a0.y + j);
		if ((d.x == _x) && (d.y == _y) && !color.isUndefined())
			mMaster.plot(a0.x, a0.y + j, color);
	}
}

Color RotationPort::peek(const size_t x, const size_t y)
{
	Point dst = rotate(x, y);
	return mMaster.peek(dst.x, dst.y);
}

Point RotationPort::rotate(const double _x, const double _y)
{
	double xsrc = _x - mCenter.x;
	double ysrc = _y - mCenter.y;
	double x, y;
	double angle = mAngle * M_PI / 180.0;
	x = xsrc * cos(angle) - ysrc * sin(angle);
	y = xsrc * sin(angle) + ysrc * cos(angle);
	x += mCenter.x;
	y += mCenter.y;
	return Point((size_t)round(x), (size_t)round(y));
}

Point RotationPort::original(double x, double y)
{
	x -= mCenter.x;
	y -= mCenter.y;
	double xsrc, ysrc;
	double angle = mAngle * M_PI / 180.0;
	double c = cos(angle);
	double s = sin(angle);
	ysrc = - (x / c - y / s) / (s / c + c / s);
	xsrc = (x / s + y / c) / (c / s + s / c);
	xsrc += mCenter.x;
	ysrc += mCenter.y;
	return Point((size_t)round(xsrc), (size_t)round(ysrc));
}
