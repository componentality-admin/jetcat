#include "fb_surface.h"

//#include "../../Surface/bmp_surface.h"
//#include "../../Surface/easy_draw.h"
//#include "../../Drawing/brush.h"
//#include "../../Drawing/viewport.h"
//#include "../../Drawing/sprite.h"
//#include "../../Fonts/character.h"
//#include "../../Fonts/FontSet.h"
//#include "../../Drawing/transparency.h"

#include "../../JetCat/label.h"
#include "../../JetCat/buttons.h"
#include "../../JetCat/placement.h"
#include "../../JetCat/window.h"

#include "../LinuxFB/touchscreenlistener.h"
#include "../LinuxFB/sdleventlistener.h"

#include "../LinuxFB/fb_surface.h"
#include "../JetCat-Modules/touch.h"
#include "../JetCat/caption.h"
#include "../JetCat/inputs.h"
#include "../../Surface/easy_draw.h"
#include "../../Drawing/viewport.h"

#include "../JetCat/imagebox.h"

#ifdef USE_SDL_FRAMEBUF
#include <SDL.h>
#endif

#include <unistd.h>
#include <iostream>

//FIXME орфография


/*
 * Виды кнопок определённых в JetCat
 * Текстовые (TextButton) - отображают текс, позволяют задать цвета надписи,фона и рамки
 * Графическая (BitmapButton) - оторбажает указанные при конструировании картинки
 * "Залипающая" (SwitchButton) - наследник от BitmapButton, работает по принцыпу переключателя
 *
 * Для обработки событий от кнопок необходимо отнаследоваться от необходимого класса и переопределить следующие функции:
 *	virtual void onPress(); - бработка нажатия
 *  virtual void onDepress(); - обработка отпуская
 *	virtual void onTimer(); - обработка истечения таймера автотпускания
 */



/*
 * На данный момент	в примерах поддерживается 2 обработчика событий Linux
 *   TouchscreenListener - обработка событий от тачскрина
 *   SDLEventListener - обработка событий мыши и тачпада
 * Для использования TouchscreenListener необходимо  раскометировать соответствуюшие строки, помечечены // << TouchscreenListener
 *     И передать путь до соответсвующего файла в /dev/input/event...
 *     Для поиска устройства установите input-utils
 *     Далее
 *        sudo input-events 1
 *        Вывод
 *        /dev/input/event1
 *        bustype : BUS_I8042
 *        vendor  : 0x2
 *		  product : 0x7
 *        version : 433
 *		  name    : "SynPS/2 Synaptics TouchPad"   <---- Название устройства
 *        phys    : "isa0060/serio1/input0"
 *		  bits ev : EV_SYN EV_KEY EV_ABS  <----- Искомое значение - EV_ABS
*/

using namespace Componentality::Graphics::JetCat;

/*
 * Класс менеджера окон поддерживающего обработку событий под Linux
 */

class WindowManager : public Componentality::Graphics::JetCat::WindowManager,
#ifdef USE_SDL_EVENTLISTENER
	public Componentality::Graphics::LinuxFB::SDLEventListener,   // << SDLEventListener
	public Componentality::Graphics::LinuxFB::SDLEventListener::Handler   // << SDLEventListener
#else
	public Componentality::Graphics::LinuxFB::TouchscreenListener,   // << TouchscreenListener
	public Componentality::Graphics::LinuxFB::TouchscreenListener::Handler   // << TouchscreenListener
#endif
{
public:
#ifdef USE_SDL_EVENTLISTENER
	WindowManager() : Componentality::Graphics::LinuxFB::SDLEventListener() // << SDLEventListener
#else
	WindowManager() : Componentality::Graphics::LinuxFB::TouchscreenListener("/dev/input/touchscreen") // << TouchscreenListener
#endif
	{
		_handler = this;
	};
	virtual ~WindowManager()
	{
	};
protected:
	virtual bool callback(int event_id, int x = 0, int y = 0, int z = 0)     // Функция для перекодировки событий из формата TouchscreenListener в форма JetCat
	{
		switch ( event_id )
		{
			case MOUSE_DOWN:
			{
				PointerEvent* event = CST_NEW(PressEvent( Componentality::Graphics::Point(x, y) ), "WindowManager", "press");    // Создаём событие "нажата на элемент"
				sendEvent(*event);    // Отправка события в JetCat
				break;
			}
			case MOUSE_UP:
			{
				PointerEvent* event = CST_NEW(DepressEvent( Componentality::Graphics::Point(x, y) ), "WindowManager", "depress");
				sendEvent(*event);
				break;
			}
		}
		return true;
	};
};

//------------------------------------------------------
//Текстовая кнопка. Она завершает работу данного примера
class ExitButton : public TextButton
{
public:
	ExitButton() : TextButton("EXIT - text button",
								  Componentality::Graphics::Color(255, 255, 255),
								  Componentality::Graphics::Color(0, 0, 100),
								  Componentality::Graphics::Color(155, 0, 0)) {}
	~ExitButton() {}
protected:
	void onDepress()    // Переопределяем обработчик отпускания кнопки
	{
		TextButton::onDepress();    // Не забываем вызвать обработчик родительского класса, иначе не будет анимации нажатия
		quick_exit( 0 );
	}
};
//------------------------------------------------------
// Класс ButtonIndication позволяет более наглядно продемонстрировать различие в работе графических кнопок.
// данный код выводит графические примитивы, они рассмотрены в примере Shapes
class Indication
{
public:
	Indication(int x, int y, Componentality::Graphics::Drawer *drawer)
	{
		this->drawer = drawer;
		this->x = x;
		this->y = y;
		shiftInd = 20;
		sizeInd = 20;
		drawer->circle( Componentality::Graphics::Point(x+shiftInd, y+sizeInd), sizeInd,  Componentality::Graphics::Color(100, 100, 100)  );
	}
	~Indication(){}

	void indication_on()
	{
		drawer->filled_circle( Componentality::Graphics::Point(x+shiftInd, y+sizeInd), sizeInd,  Componentality::Graphics::Color(0,200,0)  );
	}

	void indication_off()
	{
		drawer->filled_circle( Componentality::Graphics::Point(x+shiftInd, y+sizeInd), sizeInd,  Componentality::Graphics::Color(0, 0, 0)  );
		drawer->circle( Componentality::Graphics::Point(x+shiftInd, y+sizeInd), sizeInd,  Componentality::Graphics::Color(100, 100, 100)  );
	}

private:
	Componentality::Graphics::Drawer *drawer;
	int x;
	int y;
	int sizeInd;
	int shiftInd;
};

//------------------------------------------------------
//Графическая кнопка
class Button : public BitmapButton, public Indication
{
public:
	Button(int x, int y, Componentality::Graphics::Drawer *drawer)
		: BitmapButton( "/var/componentality/Examples/icons/BitmapButton.bmp",
						"/var/componentality/Examples/icons/BitmapButtonPressed.bmp",
						"/var/componentality/Examples/icons/BitmapButtonPassed.bmp" ),
		  Indication(x, y, drawer) {}

	~Button() {}
protected:
	void onPress()    //Обработка нажатия
	{
		BitmapButton::onPress();
		indication_on();    // см. класс ButtonIndication
	}
	void onDepress()    //Обработака отпускания
	{
		BitmapButton::onDepress();
		indication_off();
	}
};
//------------------------------------------------------
//"Залипающая" графическая кнопка
class SwButton : public SwitchButton, public Indication
{
public:
	SwButton( int x, int y, Componentality::Graphics::Drawer *drawer )
		: SwitchButton("/var/componentality/Examples/icons/SwitchButtonOn.bmp",
					   "/var/componentality/Examples/icons/SwitchButtonOff.bmp",
					   "/var/componentality/Examples/icons/SwitchButtonPassive.bmp" ),
		Indication(x, y, drawer) {}
	~SwButton() {}
protected:
	void onPress() //Обработка нажатия
	{
		SwitchButton::onPress();
		// Поскольку у данной кнопки вся  реакция происходит на нажатие,
		// то необходимо определять её состояние, с этим поможет SwitchButton::mOff - bool
		if( mOff )
			indication_off();
		else
			indication_on();
	}
};
//------------------------------------------------------
int main(int argc, char* argv[])
{
	LinuxFrameBuffer(framebuffer); //Через макрос объявлется объекта "поверхность", в зависимости от выбранного типа при

	int sizeX = 0;
	int sizeY = 0;
	int x = 0;
	int y = 0;


#ifdef USE_SDL_FRAMEBUF
	SDL_ShowCursor(SDL_ENABLE);//Включаем отображение курperror("input devices not opened");сора
#endif

	::WindowManager* DEFAULT_WINDOW_MANAGER = NULL;   // Оконный менеджер

	Componentality::Graphics::Drawer drawer(framebuffer);   // В этом примере используется только для более наглядной демонстрации

	Placement *bmpButtonPlacement = CST_NEW(Placement, "root", "placement");    // Размер и положение кнопки определяется контейнером Placement
	sizeX = 200;
	sizeY = 50;
	x = framebuffer.getWidth()/2 - sizeX/2;
	y = framebuffer.getHeight()/2 - sizeY/2;;

	bmpButtonPlacement->size( sizeX, sizeY);// Задаём размер, иначе размер будетвыбкраться DEFAULT_WINDOW_MANAGER. И все настройки выравнивания текста будут работать
											// относительно автоматически выбранного размера
	bmpButtonPlacement->move( x, y);
	Button bitmapButton( x+sizeX, y, &drawer );
	bmpButtonPlacement->add( "textButton", bitmapButton);




	Placement *swButtonPlacement = CST_NEW(Placement, "root", "placement");
	sizeX = 200;
	x = framebuffer.getWidth()/2 - sizeX/2;
	y += sizeY+10;
	sizeY = 50;
	swButtonPlacement->move(x, y);
	swButtonPlacement->size(sizeX, sizeY);
	SwButton switchButton( x+sizeX, y, &drawer );
	switchButton.set( true );
	swButtonPlacement->add( "textButton", switchButton);


	sizeX = 200;
	x = framebuffer.getWidth()/2 - sizeX/2;
	y += sizeY+20;
	sizeY = 50;
	Placement *textButtonPlacement = CST_NEW(Placement, "root", "placement");
	textButtonPlacement->size(sizeX, sizeY);
	textButtonPlacement->move(x, y);
	ExitButton textButton;
	textButtonPlacement->add( "textButton", textButton ) ;


	DEFAULT_WINDOW_MANAGER = CST_NEW(::WindowManager, "WindowManager", "instance");
	DEFAULT_WINDOW_MANAGER->setSurface( framebuffer );// Устанавливаем менеджеру окон поверхность для рисования

	//Создаём окно, и указваем менеджера окон, который будет управлять окном
	Componentality::Graphics::JetCat::Window& win = *CST_NEW(Componentality::Graphics::JetCat::Window(*DEFAULT_WINDOW_MANAGER), "root", "window");
	win.add("textButton", *textButtonPlacement); // Добавляем контейнер в окно
	win.add("bitmapButton", *bmpButtonPlacement); // Добавляем контейнер в окно
	win.add("switchButton", *swButtonPlacement); // Добавляем контейнер в окн



	DEFAULT_WINDOW_MANAGER->draw(); // Отрисовываем

#ifdef USE_SDL_FRAMEBUF
	//Если собранно с использование SDL, то немного задержимся, чтобы посмотреть на результат
	for(;;){
#ifdef USE_SDL_EVENTLISTENER
		DEFAULT_WINDOW_MANAGER->eventPool();
#endif
		CST::Common::sleep(10);
	}
#endif

	return 0;
}
