#include "fb_surface.h"

//#include "../../Surface/bmp_surface.h"
//#include "../../Surface/easy_draw.h"
//#include "../../Drawing/brush.h"
//#include "../../Drawing/viewport.h"
//#include "../../Drawing/sprite.h"
//#include "../../Fonts/character.h"
//#include "../../Fonts/FontSet.h"
//#include "../../Drawing/transparency.h"

#include "../../JetCat/label.h"
#include "../../JetCat/buttons.h"
#include "../../JetCat/placement.h"
#include "../../JetCat/window.h"
#include "../LinuxFB/touchscreenlistener.h"
#include "../LinuxFB/fb_surface.h"
#include "../JetCat-Modules/touch.h"
#include "../JetCat/caption.h"
#include "../JetCat/inputs.h"

#ifdef USE_SDL_FRAMEBUF
#include <SDL.h>
#endif

#include <unistd.h>
#include <iostream>

//FIXME орфография
//TODO описание

using namespace Componentality::Graphics::JetCat;

int main(int argc, char* argv[])
{
	LinuxFrameBuffer(framebuffer); // Через макрос объявлется объекта "поверхность",
								   // в зависимости от выбранного типа при омпиляции ( SDLFrameBuffer / LinuxFB::FrameBuffer)

#ifdef USE_SDL_FRAMEBUF
	SDL_ShowCursor(SDL_ENABLE);//Включаем отображение курсора
#endif

	/*
	 * JetCat реализует два типа окон:
	 *   основные окна (window) - базовые окна, размер и положение соответствует "поверхности", на которую рисует менеджер окон
	 *   модальные окна - отличаются тем, что можно менять размер и положение
	 *
	 * Поддерживается создание нескольких окон.
	 * Чем познее создан объект окна, тем выше он находится в графическом слое.
	 * Переключение между окна можно осуществлять при помощи функций Window::hide() / Window::show().
	 *   Одна необходимо помнить - если окно находится на слое ниже, то при вызове только функции Window::show(),
	 *   оно останется перекрыто окнами, расположенными выше.
	 */

	WindowManager* DEFAULT_WINDOW_MANAGER = NULL;     // Оконный менеджер

	DEFAULT_WINDOW_MANAGER = CST_NEW(WindowManager, "WindowManager", "instance");
	DEFAULT_WINDOW_MANAGER->setSurface( framebuffer );    // Устанавливаем менеджеру окон поверхность для рисования


	///------  Основное окно  ------------------------

	//Создаём окно, и указваем менеджера окон, который будет управлять окном
	Window& window = *CST_NEW(Window(*DEFAULT_WINDOW_MANAGER), "WindowManager", "Window");

	Caption& caption = *CST_NEW(Caption, "Window", "caption");     // Создаём объект заголовка окна
	caption.setTitle("Main Window");
	window.add("title", caption);    // Добавляем окну заголовок
	Background& background = *CST_NEW(Background, "Window", "background");     // Создаём объект фона
	background.setColor( Componentality::Graphics::Color(100, 100, 150) );
	window.add("background", background);	  // Устанавливаем фон


	///------  Модальное окно  ------------------------

	ModalWindow& modalWin = *CST_NEW(ModalWindow( *DEFAULT_WINDOW_MANAGER), "Window", "modal");  // Объект модально окна
	Caption& mwCaption = *CST_NEW(Caption, "Window", "caption");
	mwCaption.setTitle( "Modal Window");
	modalWin.add( "caption", mwCaption);    // Указываем заголовок
	Background& mwBackground = *CST_NEW(Background, "window", "background");
	mwBackground.setColor( Componentality::Graphics::Color( 200, 200, 0) );
	modalWin.add("background", mwBackground);    //Указываем фон
	Frame & mwFrame = *CST_NEW(Frame, "Window", "frame");    // Создаём объект обрамления
	modalWin.add( "frame", mwFrame);    // Указываем обромление

	modalWin.size( 300, 200);    // Заём размер модального окна
	modalWin.move( 100, 100);    // Указываем позицию

	DEFAULT_WINDOW_MANAGER->draw();    // Отрисовываем, перерисовывать необходимо при каждом изменении


#ifdef USE_SDL_FRAMEBUF
	//Если собранно с использование SDL, то немного задержимся, чтобы посмотреть на результат
	CST::Common::sleep( 2000);
#endif

	return 0;
}
