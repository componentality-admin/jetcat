#include "fb_surface.h"

//#include "../../Surface/bmp_surface.h"
//#include "../../Surface/easy_draw.h"
//#include "../../Drawing/brush.h"
//#include "../../Drawing/viewport.h"
//#include "../../Drawing/sprite.h"
//#include "../../Fonts/character.h"
//#include "../../Fonts/FontSet.h"
//#include "../../Drawing/transparency.h"

#include "../../JetCat/label.h"
#include "../../JetCat/buttons.h"
#include "../../JetCat/placement.h"
#include "../../JetCat/window.h"
#include "../LinuxFB/touchscreenlistener.h"
#include "../LinuxFB/sdleventlistener.h"
#include "../LinuxFB/fb_surface.h"
#include "../JetCat-Modules/touch.h"
#include "../JetCat/caption.h"
#include "../JetCat/inputs.h"
#include "../JetCat/menu.h"

#include "../JetCat/imagebox.h"

#ifdef USE_SDL_FRAMEBUF
#include <SDL.h>
#endif

#include <unistd.h>
#include <iostream>
//FIXME орфография
//TODO обработка мыши, SDL?
//TODO обработка событий
//TODO описание

/*
поиск необходимого устройства
sudo apt-get install input-utils
sudo input-events 1
/dev/input/event1
   bustype : BUS_I8042
   vendor  : 0x2
   product : 0x7
   version : 433
   name    : "SynPS/2 Synaptics TouchPad"   <---- Название устройства
   phys    : "isa0060/serio1/input0"
   bits ev : EV_SYN EV_KEY EV_ABS  <----- Искомое значение - EV_ABS
*/

using namespace Componentality::Graphics::JetCat;
using namespace Componentality::Graphics;

class WindowManager : public Componentality::Graphics::JetCat::WindowManager,
		#ifdef USE_SDL_EVENTLISTENER
			public Componentality::Graphics::LinuxFB::SDLEventListener,   // << SDLEventListener
			public Componentality::Graphics::LinuxFB::SDLEventListener::Handler   // << SDLEventListener
		#else
			public Componentality::Graphics::LinuxFB::TouchscreenListener,   // << TouchscreenListener
			public Componentality::Graphics::LinuxFB::TouchscreenListener::Handler   // << TouchscreenListener
		#endif
{
public:
#ifdef USE_SDL_EVENTLISTENER
	WindowManager() : Componentality::Graphics::LinuxFB::SDLEventListener() // << SDLEventListener
#else
	WindowManager() : Componentality::Graphics::LinuxFB::TouchscreenListener("/dev/input/touchscreen") // << TouchscreenListener
#endif
	{
		_handler = this;
	};
	virtual ~WindowManager()
	{
	};
protected:
	virtual bool callback(int event_id, int x = 0, int y = 0, int z = 0)
	{
		//printf("Event %d, X=%d, Y=%d, Z=%d\n", event_id, x, y, z);
		switch ( event_id )
		{
			case MOUSE_DOWN:
			{
				PointerEvent* event = CST_NEW(PressEvent( Componentality::Graphics::Point(x, y) ), "root", "pointer event");
				sendEvent(*event);
				break;
			}
			case MOUSE_UP:
			{
				PointerEvent* event = CST_NEW(DepressEvent( Componentality::Graphics::Point(x, y) ), "root", "pointer event");
				sendEvent(*event);
				break;
			}
		}
		return true;
	};
};

class ExitButton : public TextButton
{
public:
	ExitButton() : TextButton("EXIT",
							  Componentality::Graphics::Color(255, 255, 255),
							  Componentality::Graphics::Color(0, 0, 100),
							  Componentality::Graphics::Color(155, 0, 0)) {}
	~ExitButton() {}
protected:
	void onDepress()
	{
		TextButton::onDepress();
		quick_exit( 0 );
	}
};

//------------------------------------------------------

#include <stdio.h>

class Indication
{
public:
	Indication(int x, int y, Componentality::Graphics::Drawer *drawer)
	{
		this->drawer = drawer;
		this->x = x;
		this->y = y;
		shiftInd = 20;
		sizeInd = 20;
		drawer->circle( Componentality::Graphics::Point(x+shiftInd, y+sizeInd), sizeInd,  Componentality::Graphics::Color(100, 100, 100)  );
	}
	~Indication(){}

	void indication_on()
	{
		drawer->filled_circle( Componentality::Graphics::Point(x+shiftInd, y+sizeInd), sizeInd,  Componentality::Graphics::Color(200,200,0)  );
	}

	void indication_off()
	{
		drawer->filled_circle( Componentality::Graphics::Point(x+shiftInd, y+sizeInd), sizeInd,  Componentality::Graphics::Color(0, 0, 0)  );
		drawer->circle( Componentality::Graphics::Point(x+shiftInd, y+sizeInd), sizeInd,  Componentality::Graphics::Color(100, 100, 100)  );
	}

private:
	Componentality::Graphics::Drawer *drawer;
	int x;
	int y;
	int sizeInd;
	int shiftInd;
};


//Рассмотрим пример создания собственных элементов меню на примере элемента содержащего картинку
class IImageMenuItem : public IMenuItem
{
public:
   IImageMenuItem ( IMenu& menu, const std::string imagePath ) : IMenuItem(menu)
   {
	   mImage.setFilename(imagePath);
	   this->add("image", mImage);    // Добавляем артинку
   }

   ~IImageMenuItem()
   {

   }

protected:
   Image mImage;

   virtual void onSelected() = 0;   // Обработчик выбора элемента меню

   virtual bool onEvent(CST::Common::Event& event)    // Переопределяем обработку событий
   {
	   if (!filter(event))     // Если событие не относится к нашему типу элементов, то пропускаем
		   return false;
	   if (event.getType() == EVENT_TYPE_PRESS)   // Если событие - нажатие на menu item
	   {
		   onSelected();    // Вызываеем наш обработчик

		   raise(*CST_NEW(MenuEvent(this), "root", "menu event"));
		   event.setProcessed();
		   return true;
	   }
	   return IElement::onEvent(event);    // Передаём событие дальше
   }
};


// Класс рафического элемента меню отвечающего за включение "лампочки"
class ImageItemIndicationOn : public IImageMenuItem, public Indication
{
public:
	ImageItemIndicationOn ( IMenu& menu, const std::string imagePath, int x, int y,  Componentality::Graphics::Drawer *drawer)
		: IImageMenuItem( menu, imagePath),
		  Indication( x, y, drawer)
		{}
	~ImageItemIndicationOn () {}

protected:
   virtual void onSelected(){    // Переопределяем и реализуем обработчик
	  indication_on();
	}
};

class ImageItemIndicationOff : public IImageMenuItem,  public Indication
{
public:
	ImageItemIndicationOff ( IMenu& menu, const std::string imagePath, int x, int y,  Componentality::Graphics::Drawer *drawer)
		: IImageMenuItem( menu, imagePath),
		  Indication( x, y, drawer)
		{}
	~ImageItemIndicationOff () {}

protected:
   virtual void onSelected(){
		indication_off();
	}
};

int main(int argc, char* argv[])
{
	LinuxFrameBuffer(framebuffer); //Через макрос объявлется объекта "поверхность", в зависимости от выбранного типа при

	Componentality::Graphics::Drawer drawer(framebuffer);   // В этом примере используется только для более наглядной демонстрации

#ifdef USE_SDL_FRAMEBUF
	SDL_ShowCursor(SDL_ENABLE);//Включаем отображение курperror("input devices not opened");сора
#endif

	SDL_ShowCursor(SDL_ENABLE);//Включаем отображение курсора

	::WindowManager* DEFAULT_WINDOW_MANAGER = CST_NEW(::WindowManager, "window manager", "instance");    // Оконный менеджер

	//Создаём окно, и указваем менеджера окон, который будет управлять окном
	Componentality::Graphics::JetCat::Window& win = *CST_NEW(Componentality::Graphics::JetCat::Window(*DEFAULT_WINDOW_MANAGER), "window manager", "window");

	//----- Вертикальное меню -----

	Placement& menuholder = *CST_NEW(Placement, "root", "placement");
	VerticalMenu &menu = *CST_NEW(VerticalMenu(*DEFAULT_WINDOW_MANAGER, 150), "root", "vertical menu");    //	Создаём объект вертикально меню, с указанием размера по горизонтали

	// Далее идёт создание menu item
	// Обратите внимание, необходимо создать два элемента, для каждого пункта меню
	// первый - выбранный элемент
	// второй - не выбранный
	TextMenuItem& item1_unsel = *CST_NEW(TextMenuItem(menu,
		Color(255, 255, 255), Color(20, 20, 20), "Item 1"), "root", "item");
	TextMenuItem& item1_sel = *CST_NEW(TextMenuItem(menu,
		Color(0, 0, 0), Color(255, 255, 255), "Item 1"), "root", "item");
	TextMenuItem& item2_unsel = *CST_NEW(TextMenuItem(menu,
		Color(255, 255, 255), Color(20, 20, 20), "Item 2"), "root", "item");
	TextMenuItem& item2_sel = *CST_NEW(TextMenuItem(menu,
		Color(0, 0, 0), Color(255, 255, 255), "Item 2"), "root", "item");
	TextMenuItem& item3_unsel = *CST_NEW(TextMenuItem(menu,
		Color(255, 255, 255), Color(20, 20, 20), "Item 3"), "root", "item");
	TextMenuItem& item3_sel = *CST_NEW(TextMenuItem(menu,
		Color(0, 0, 0), Color(255, 255, 255), "Item 3"), "root", "item");
	TextMenuItem& item4_unsel = *CST_NEW(TextMenuItem(menu,
		Color(255, 255, 255), Color(20, 20, 20), "Item 4"), "root", "item");
	TextMenuItem& item4_sel = *CST_NEW(TextMenuItem(menu,
		Color(0, 0, 0), Color(255, 255, 255), "Item 4"), "root", "item");

	menu.add("item 1", item1_unsel, item1_sel, 40); //Добавляем элементы меню
	menu.add("item 2", item2_unsel, item2_sel, 40);
	menu.add("item 3", item3_unsel, item3_sel, 40);
	menu.add("item 4", item4_unsel, item4_sel, 40);
	menu.select("item 3");    // Указываем по умолчанию выбранный элемент
	menuholder.add("menu", menu);    // Помещаем меню в Placement
	menuholder.move(0, 30);
	menuholder.size(100, 200);

	win.add("menuholder", menuholder);    // Добавляем в окно Placement

	//----- Горизонтальное меню -----

	Placement& hmenuholder = *CST_NEW(Placement, "root", "placement");
	IHorizontalMenu &hmenu = *CST_NEW(HorizontalMenu(*DEFAULT_WINDOW_MANAGER, 30), "root", "horizontal menu");
	TextMenuItem& hitem1_unsel = *CST_NEW(TextMenuItem(hmenu,
		Color(255, 255, 255), Color(20, 20, 20), "Item 1"), "root", "item");
	TextMenuItem& hitem1_sel = *CST_NEW(TextMenuItem(hmenu,
		Color(0, 0, 0), Color(255, 255, 255), "Item 1"), "root", "item");
	TextMenuItem& hitem2_unsel = *CST_NEW(TextMenuItem(hmenu,
		Color(255, 255, 255), Color(20, 20, 20), "Item 2"), "root", "item");
	TextMenuItem& hitem2_sel = *CST_NEW(TextMenuItem(hmenu,
		Color(0, 0, 0), Color(255, 255, 255), "Item 2"), "root", "item");
	TextMenuItem& hitem3_unselCST_NEW(TextMenuItem(hmenu,
		Color(255, 255, 255), Color(20, 20, 20), "Item 3"), "root", "item");
	TextMenuItem& hitem3_sel = *CST_NEW(TextMenuItem(hmenu,
		Color(0, 0, 0), Color(255, 255, 255), "Item 3"), "root", "item");
	hmenuholder.add("hmenu", hmenu);
	hmenu.add("item 1", hitem1_unsel, hitem1_sel, 100);
	hmenu.add("item 2", hitem2_unsel, hitem2_sel, 200);
	hmenu.add("item 3", hitem3_unsel, hitem3_sel, 300);
	hmenu.select("item 2");
	hmenuholder.move(100, 0);
	hmenuholder.size(600, 30);

	win.add("hmenuholder", hmenuholder);



	//------------ Графическое меню ----------------------

	Placement& ImagesMenuHolder = *CST_NEW(Placement, "root", "placement";
	IVerticalMenu &ImagesMenu = *CST_NEW(VerticalMenu(*DEFAULT_WINDOW_MANAGER, 150), "root", "vertical menu");

	ImageItemIndicationOn& imageItemOnUnsel = *CST_NEW(ImageItemIndicationOn(menu,
																			"/var/componentality/Examples/icons/MenuOnUnselect.bmp",
																			70, 225, &drawer), "root", "");
	ImageItemIndicationOn& imageItemOnsel = *CST_NEW(ImageItemIndicationOn(menu,
																		  "/var/componentality/Examples/icons/MenuOnSelect.bmp",
																		  70, 225, &drawer), "root", "");
	ImageItemIndicationOff& imageItemOffUnsel = *CST_NEW(ImageItemIndicationOff(menu,
																			  "/var/componentality/Examples/icons/MenuOffUnselect.bmp",
																			  70, 225, &drawer), "root", "");
	ImageItemIndicationOff& ImageItemOffSel = *CST_NEW(ImageItemIndicationOff(menu,
																			"/var/componentality/Examples/icons/MenuOffSelect.bmp",
																			70, 225, &drawer), "root", "");
	ImagesMenu.add("bitmap item on", imageItemOnUnsel, imageItemOnsel, 30);
	ImagesMenu.add("bitmap item off", imageItemOffUnsel, ImageItemOffSel, 30);
	ImagesMenuHolder.add("bitmapmenu", ImagesMenu);
	ImagesMenuHolder.move(0, 220);
	ImagesMenuHolder.size(150, 60);

	win.add("imagemenuholder", ImagesMenuHolder);

	//--------------- Кнопка выхода ----------------

	Placement *textButtonPlacement = CST_NEW(Placement(), "root", "placement"); // Объявляем класс Placement
	textButtonPlacement->size(100, 50);// Задаём размер, иначе размер будетвыбкраться DEFAULT_WINDOW_MANAGER. И все настройки выравнивания текста будут работать
							 // относительно автоматически выбранного размера
	textButtonPlacement->move(600, 280); // Смешаем placement во второю половину она по оси Х
	ExitButton textButton;
	textButtonPlacement->add( "textButton", textButton ) ;

	DEFAULT_WINDOW_MANAGER->setSurface( framebuffer );// Устанавливаем менеджеру окон поверхность для рисования

	win.add("exit", *textButtonPlacement);



	DEFAULT_WINDOW_MANAGER->draw(); // Отрисовываем


#ifdef USE_SDL_FRAMEBUF
	//Если собранно с использование SDL, то немного задержимся, чтобы посмотреть на результат
	for(;;)
		for(;;){
	#ifdef USE_SDL_EVENTLISTENER
			DEFAULT_WINDOW_MANAGER->eventPool();
	#endif
			CST::Common::sleep(10);
		}
#endif

	return 0;
}
