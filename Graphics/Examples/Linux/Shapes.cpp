#include "fb_surface.h"

#include "../../Surface/bmp_surface.h"
#include "../../Surface/easy_draw.h"
#include "../../Drawing/brush.h"
#include "../../Drawing/viewport.h"
#include "../../Drawing/sprite.h"
#include "../../Fonts/character.h"
#include "../../Fonts/FontSet.h"
#include "../../Drawing/transparency.h"


//FIXME орфография

int main(int argc, char* argv[])
{
	LinuxFrameBuffer(framebuffer); //Через макрос объявлется объекта "поверхность", в зависимости от выбранного типа при компиляции
	Componentality::Graphics::Drawer drawer(framebuffer); //Создаём обект для рисования простых примитивов

	//Рисуем залитый прямоугольник, Совпадающий по размерам с "поверхностью"
	drawer.filled_rectangle(Componentality::Graphics::Point(0, 0),
							Componentality::Graphics::Point(framebuffer.getWidth() - 1, framebuffer.getHeight() - 1),
							Componentality::Graphics::Color(0, 0, 10));

	//Рисуем точьки заданного цвета
	for(int x=0; x<10; x++){
		drawer.point(Componentality::Graphics::Point(x*10, 10), Componentality::Graphics::Color(255, 255, 255));
	}

	//Линия
	drawer.line(Componentality::Graphics::Point(50, 50), Componentality::Graphics::Point(250, 250), Componentality::Graphics::Color(255, 0, 0));
	//Не залитый прямоуголиник
	drawer.rectangle(Componentality::Graphics::Point(50, 50), Componentality::Graphics::Point(250, 250), Componentality::Graphics::Color(0, 255, 0));
	//Залитый прямоугольник
	drawer.filled_rectangle(Componentality::Graphics::Point(350, 350), Componentality::Graphics::Point(550, 550), Componentality::Graphics::Color(0, 0, 255));
	//Круг
	drawer.circle(Componentality::Graphics::Point(150, 150), 50, Componentality::Graphics::Color(0, 255, 0));
	//Залитый круг
	drawer.filled_circle(Componentality::Graphics::Point(450, 450), 50, Componentality::Graphics::Color(255, 255, 255));

#ifdef USE_SDL_FRAMEBUF
	//Если собранно с использование SDL, то немного задержимся, чтобы посмотреть на результат
	CST::Common::sleep(5000);
#endif

	return 0;
}
