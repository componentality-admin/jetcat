#include "fb_surface.h"

#include "../../Surface/bmp_surface.h"
#include "../../Surface/easy_draw.h"
#include "../../Drawing/brush.h"
#include "../../Drawing/viewport.h"
#include "../../Drawing/sprite.h"
#include "../../Fonts/character.h"
#include "../../Fonts/FontSet.h"
#include "../../Drawing/transparency.h"

#include "../../JetCat/label.h"
#include "../../JetCat/placement.h"
#include "../../JetCat/window.h"

#include "iostream"


//FIXME орфография

using namespace Componentality::Graphics::JetCat;

int main(int argc, char* argv[])
{
	LinuxFrameBuffer(framebuffer); //Через макрос объявлется объекта "поверхность", в зависимости от выбранного типа при компиляции


	/*
	 * Вывод текста осуществляется при помощи класса Componentality::Graphics::JetCat::Label и производных от него
	 * Label спользует для вывода StandardTextOutput - вычисляет его длину, усекает до влезающего количества символов + многоточие.
	 *    Соответственно, тратит больше времени, но делает вывод дружественнее.
	 * Производные:
	 *  EasyLabel - по сравнению с Label, использует для вывода EasyTextOutput - обрезает текст по границе элемента,
	 *  MultilineLabel - позволяет выводить многострочные тексте
	 * Положение текста определяется выравниванием, для задания произвольной позиции необходимо использовать Componentality::Graphics::JetCat::Placement
	 *	Описание идёт за секцией "Полдожение задаётся выравниванием"
	 */


	//----	Полдожение задаётся выравниванием   --------------------------------------------------------

	// Обычный Label
	Componentality::Graphics::JetCat::Label label;
	label.setColor(Componentality::Graphics::Color(100, 255, 10));//Задаём цвет RGB
	//Задаём выравнивание по горизонтали и вертикали
	label.setAlignment( std::pair<ITextOutput::HorizontalAlignment, ITextOutput::VerticalAlignment>(ITextOutput::H_LEFT, ITextOutput::V_TOP));
	label.setText("H_LEFT, V_TOP");
	label.draw(framebuffer);//Отрисовываем label на поверхности framebufer

	//Используем заранее созданный label и изменем его выравнивание
	label.setAlignment( std::pair<ITextOutput::HorizontalAlignment, ITextOutput::VerticalAlignment>(ITextOutput::H_RIGHT, ITextOutput::V_BOTTOM));
	label.setText("H_RIGHT, V_BOTTOM");
	label.draw(framebuffer);

	// Вывод многострочного текста
	Componentality::Graphics::JetCat::MultilineLabel multilineLabel;
	multilineLabel.setColor(Componentality::Graphics::Color(200, 100, 200));
	multilineLabel.setAlignment( std::pair<ITextOutput::HorizontalAlignment, ITextOutput::VerticalAlignment>(ITextOutput::H_CENTER, ITextOutput::V_CENTER));
	multilineLabel.setText("MultilineLabel\n\nH_CENTER\nV_CENTER");//Строки разделяются символом '\n'
	multilineLabel.draw(framebuffer);


	// Изменение шрифта
	Componentality::Graphics::JetCat::EasyLabel labelCalibri("Font: Calibri 32 bold", Componentality::Graphics::Color(0, 0, 200) );
	labelCalibri.setFont( Componentality::Graphics::FontSet::FontDescriptor("Calibri", 32, Componentality::Graphics::Font::FONT_BOLD) );// Задаём шрифт
	labelCalibri.draw(framebuffer);

	Componentality::Graphics::JetCat::EasyLabel labelArial("Font: Arial 24 italic", Componentality::Graphics::Color(200, 200, 200) );
	labelArial.setAlignment( std::pair<ITextOutput::HorizontalAlignment, ITextOutput::VerticalAlignment>(ITextOutput::H_LEFT, ITextOutput::V_BOTTOM));
	labelArial.setFont( Componentality::Graphics::FontSet::FontDescriptor("Arial", 24, Componentality::Graphics::Font::FONT_ITALIC) );
	labelArial.draw(framebuffer);


	//----	Произвольное положение текста   --------------------------------------------------------

	/*
	 * Для задания положения текста используется Placement - контейнер для граффических элементов, позволяющий задать размер и смещение
	 * Сам по себе Placement отрисоваться на поверхности не может, его отрисовкой управляет оконный менеджер, по средствам окна
	 */
	Componentality::Graphics::JetCat::WindowManager* DEFAULT_WINDOW_MANAGER = NULL; // Оконный менеджер

	Componentality::Graphics::JetCat::Label freePosLabel("Free Position Label", Componentality::Graphics::Color(255, 255, 255) );
	freePosLabel.setAlignment( std::pair<ITextOutput::HorizontalAlignment, ITextOutput::VerticalAlignment>(ITextOutput::H_LEFT, ITextOutput::V_CENTER));
	freePosLabel.setFont( Componentality::Graphics::FontSet::FontDescriptor("Arial", 24, Componentality::Graphics::Font::FONT_NORMAL) );

	Placement *placement = CST_NEW(Placement, "root", "placement"); // Объявляем класс Placement
	placement->size(200, 50);// Задаём размер, иначе размер будетвыбкраться DEFAULT_WINDOW_MANAGER. И все настройки выравнивания текста будут работать
							 // относительно автоматически выбранного размера
	int x = framebuffer.getWidth()/2;
	placement->move(x+x/2, 200); // Смешаем placement во второю половину она по оси Х
	placement->add("freelabel", freePosLabel); // Помещаем label в placement. Порядок добовления влияет на "слой"

	DEFAULT_WINDOW_MANAGER = CST_NEW(WindowManager, "root", "WindowManager");
	DEFAULT_WINDOW_MANAGER->setSurface( framebuffer );// Устанавливаем менеджеру окон поверхность для рисования

	//Создаём окно, и указваем менеджера окон, который будет управлять окном
	Componentality::Graphics::JetCat::Window& win = *CST_NEW(Componentality::Graphics::JetCat::Window(*DEFAULT_WINDOW_MANAGER), "root", "window");
	win.add("freelabel", *placement); // Добавляем контейнер в окно

	DEFAULT_WINDOW_MANAGER->draw(); // Отрисовываем

#ifdef USE_SDL_FRAMEBUF
	//Если собранно с использование SDL, то немного задержимся, чтобы посмотреть на результат
	CST::Common::sleep(5000);
#endif

	return 0;
}
