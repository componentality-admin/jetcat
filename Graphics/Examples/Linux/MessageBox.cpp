#include "fb_surface.h"

//#include "../../Surface/bmp_surface.h"
//#include "../../Surface/easy_draw.h"
//#include "../../Drawing/brush.h"
//#include "../../Drawing/viewport.h"
//#include "../../Drawing/sprite.h"
//#include "../../Fonts/character.h"
//#include "../../Fonts/FontSet.h"
//#include "../../Drawing/transparency.h"

#include "../../JetCat/window.h"
#include "../LinuxFB/fb_surface.h"
#include "../../JetCat/messagebox.h"
#include "../../Fonts/FontSet.h"

#ifdef USE_SDL_FRAMEBUF
#include <SDL.h>
#endif

//FIXME орфография
//TODO описание

using namespace Componentality::Graphics::JetCat;

int main(int argc, char* argv[])
{
	LinuxFrameBuffer(framebuffer); // Через макрос объявлется объекта "поверхность",
								   // в зависимости от выбранного типа при компиляции ( SDLFrameBuffer / LinuxFB::FrameBuffer)

#ifdef USE_SDL_FRAMEBUF
	SDL_ShowCursor(SDL_ENABLE);//Включаем отображение курсора
#endif

	/*
	 *   MessageWindow является наследником класса ModalWindow и имеет следующие отличия:
	 *   * функции для вывода текста сообщения и измения егошрифта
	 *	 * функции для добавления кнопок
	 */

	WindowManager* DEFAULT_WINDOW_MANAGER = NULL;     // Объявляем объект оконного менеджера

	DEFAULT_WINDOW_MANAGER = CST_NEW(WindowManager, "WindowManager", "instance");
	DEFAULT_WINDOW_MANAGER->setSurface( framebuffer );    // Устанавливаем менеджеру окон поверхность для рисования

	MessageWindow& messageWindow = *CST_NEW(MessageWindow(*DEFAULT_WINDOW_MANAGER, false), "WindowManager", "message window");
	messageWindow.setText( "Message text" );    // Задаём сообщение
	messageWindow.setFont( Componentality::Graphics::FontSet::FontDescriptor( "Calibri", 32, Componentality::Graphics::Font::FONT_NORMAL) );

	// Добовление кнопок
	TextButton& okButton = *CST_NEW(TextButton(
		"OK",
		Componentality::Graphics::Color(255, 255, 255),
		Componentality::Graphics::Color(0, 255, 0),
		Componentality::Graphics::Color(0, 0, 0)), "root", "OK button");
	TextButton& cancelButton = *CST_NEW(TextButton(
		"Cancel",
		Componentality::Graphics::Color(255, 255, 255),
		Componentality::Graphics::Color(255, 0, 0),
		Componentality::Graphics::Color(0, 0, 0)), "root", "Cancel button");
	messageWindow.addButton("OK", okButton); //Добавляем кнопку ок в messageWindow
	messageWindow.addButton("Cancel", cancelButton);
	messageWindow.setButtonY( 150 );

	DEFAULT_WINDOW_MANAGER->draw();    // Отрисовываем, перерисовывать необходимо при каждом изменении


#ifdef USE_SDL_FRAMEBUF
	//Если собранно с использование SDL, то немного задержимся, чтобы посмотреть на результат
	CST::Common::sleep( 2000);
#endif

	return 0;
}
