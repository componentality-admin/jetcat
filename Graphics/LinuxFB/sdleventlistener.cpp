#include "sdleventlistener.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <errno.h>
#include <sys/select.h>
#include <unistd.h>

#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#include <SDL.h>
#include <iostream>



using namespace CST::Common;
using namespace Componentality::Graphics::LinuxFB;

SDLEventListener::SDLEventListener( )
{
}

SDLEventListener::~SDLEventListener()
{
}

void SDLEventListener::eventPool()
{

	SDL_Event event;

	SDL_WaitEvent( &event );

	switch(event.type)
	{
		case SDL_MOUSEBUTTONDOWN:
		{
			_handler->callback(MOUSE_DOWN, event.button.x, event.button.y);
			break;
		}
		case SDL_MOUSEBUTTONUP:
		{
			_handler->callback(MOUSE_UP, event.button.x, event.button.y);
			break;
		}
	}

	return;
}
