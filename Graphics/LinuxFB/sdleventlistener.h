#ifndef __SDLEVENTLISTENER_H__
#define __SDLEVENTLISTENER_H__

#include <string>
#include <linux/version.h>
#include <linux/input.h>
#include "../../../common-libs/common/common_utilities.h"

#include "touchscreenlistener.h"

#include <SDL.h>

namespace Componentality
{
	namespace Graphics
	{
		namespace LinuxFB
		{

			class SDLEventListener
			{
			public:
				SDLEventListener();
				~SDLEventListener();
				void eventPool();

				enum MOUSE_EVENT_TYPE
				{
					MOUSE_DOWN = 1,
					MOUSE_UP
				};

				class Handler
				{
				public:
					virtual bool callback(int event_id, int x = 0, int y = 0, int z = 0) = 0;
				};

			protected:
				Handler * _handler;
			};

		}
	}
}

#endif // TOUCHSCREENLISTENER_H
